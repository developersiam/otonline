﻿using OTOnlineMockup.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.DirectoryServices;
using OTOnlineMockup.Model;
using OTOnlineMockup.ViewModel;
using OTOnlineMockup.Service;

namespace OTOnlineMockup
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtUsername.Focus();
        }
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            getLogin();
        }
        private void txtUsername_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getLogin();
        }
        void getLogin()
        {
            try
            {
                string Username = txtUsername.Text.Trim();
                string Password = txtPassword.Password;

                if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
                {
                    MessageBox.Show("กรุณาใส่ Username และ Password.", "Warning!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    if (OTOnlineService.UserAD().Authenticate(Username, Password))
                    {
                        GlobalVariable.curentUser = OTOnlineService.DataHelper().getCurentUser(Username);
                        MainWindow window = new MainWindow();
                        window.ShowDialog();
                    }
                    else MessageBox.Show("เข้าสู่ระบบไม่สำเร็จ.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Waring!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void txtPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) getLogin();
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtPassword.Password = string.Empty;
            txtUsername.Text = string.Empty;
        }
    }
}
