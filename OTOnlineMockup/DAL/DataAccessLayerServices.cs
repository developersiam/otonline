﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.DAL
{
    public static class DataAccessLayerServices
    {
        public static IEmployeeRepository EmployeeRepository()
        {
            IEmployeeRepository obj = new EmployeeRepository(new HRISSystemEntities());
            return obj;
        }

        public static IStaffStatuRepository StaffStatuRepository()
        {
            IStaffStatuRepository obj = new StaffStatuRepository(new HRISSystemEntities());
            return obj;
        }
        public static IStaffTypeRepository StaffTypeRepository()
        {
            IStaffTypeRepository obj = new StaffTypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTRepository OTRepository()
        {
            IOTRepository obj = new OTRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTTypeRepository OTTypeRepository()
        {
            IOTTypeRepository obj = new OTTypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTRequestTypeRepository OTRequestTypeRepository()
        {
            IOTRequestTypeRepository obj = new OTRequestTypeRepository(new HRISSystemEntities());
            return obj;
        }
        public static IDepartmentRepository DepartmentRepository()
        {
            IDepartmentRepository obj = new DepartmentRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupAdminRepository OTOnlineSetupAdminRepository()
        {
            IOTOnlineSetupAdminRepository obj = new OTOnlineSetupAdminRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupAdminDetailRepository OTOnlineSetupAdminDetailRepository()
        {
            IOTOnlineSetupAdminDetailRepository obj = new OTOnlineSetupAdminDetailRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupApproverRepository OTOnlineSetupApproverRepository()
        {
            IOTOnlineSetupApproverRepository obj = new OTOnlineSetupApproverRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupApproverDetailRepository OTOnlineSetupApproverDetailRepository()
        {
            IOTOnlineSetupApproverDetailRepository obj = new OTOnlineSetupApproverDetailRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupFinalApproverRepository OTOnlineSetupFinalApproverRepository()
        {
            IOTOnlineSetupFinalApproverRepository obj = new OTOnlineSetupFinalApproverRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupFinalApproverDetailRepository OTOnlineSetupFinalApproverDetailRepository()
        {
            IOTOnlineSetupFinalApproverDetailRepository obj = new OTOnlineSetupFinalApproverDetailRepository(new HRISSystemEntities());
            return obj;
        }
        public static IOTOnlineSetupHRApproverRepository OTOnlineSetupHRApproverRepository()
        {
            IOTOnlineSetupHRApproverRepository obj = new OTOnlineSetupHRApproverRepository(new HRISSystemEntities());
            return obj;
        }
        public static ILot_NumberRepository Lot_NumberRepository()
        {
            ILot_NumberRepository obj = new Lot_NumberRepository(new STEC_PayrollEntities());
            return obj;
        }
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
        //public static I AA()
        //{
        //    I obj = new AA(new HRISSystemEntities());
        //    return obj;
        //}
    }
}
