﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.DAL
{
    public class GenericDataRepository<T> : IGenericDataRepository<T> where T : class
    {
        internal DbContext context;

        public GenericDataRepository(DbContext db)
        {
            this.context = db;
        }

        public virtual IList<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            IQueryable<T> dbQuery = context.Set<T>();

            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            list = dbQuery
                .AsNoTracking()
                .ToList<T>();
            context.Dispose();
            return list;

            //using (var context = new HRISSystemEntities())
            //{
            //    IQueryable<T> dbQuery = context.Set<T>();

            //    //Apply eager loading
            //    foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
            //        dbQuery = dbQuery.Include<T, object>(navigationProperty);

            //    list = dbQuery
            //        .AsNoTracking()
            //        .ToList<T>();
            //}
            //return list;
        }

        public virtual IList<T> GetList(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            List<T> list;
            IQueryable<T> dbQuery = context.Set<T>().Where(where);

            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            list = dbQuery
                .AsNoTracking()
                .Where(where)
                .ToList<T>();
            context.Dispose();
            return list;

            //List<T> list;
            //using (var context = new HRISSystemEntities())
            //{
            //    IQueryable<T> dbQuery = context.Set<T>().Where(where);

            //    //Apply eager loading
            //    foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
            //        dbQuery = dbQuery.Include<T, object>(navigationProperty);

            //    list = dbQuery
            //        .AsNoTracking()
            //        .Where(where)
            //        .ToList<T>();
            //}
            //return list;
        }

        public virtual T GetSingle(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] navigationProperties)
        {
            T item = null;
            IQueryable<T> dbQuery = context.Set<T>().Where(where);

            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            item = dbQuery
                .AsNoTracking() //Don't track any changes for the selected item
                .FirstOrDefault(where); //Apply where clause
            return item;

            //T item = null;
            //using (var context = new HRISSystemEntities())
            //{
            //    IQueryable<T> dbQuery = context.Set<T>().Where(where);

            //    //Apply eager loading
            //    foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
            //        dbQuery = dbQuery.Include<T, object>(navigationProperty);

            //    item = dbQuery
            //        .AsNoTracking() //Don't track any changes for the selected item
            //        .FirstOrDefault(where); //Apply where clause
            //}
            //return item;
        }

        /* rest of code omitted */

        public void Add(params T[] items)
        {
            //using (var context = new HRISSystemEntities())
            //{
            foreach (T item in items)
            {
                context.Entry(item).State = EntityState.Added;
            }
            context.SaveChanges();
            //}
        }

        public void Update(params T[] items)
        {
            //using (var context = new HRISSystemEntities())
            //{
            foreach (T item in items)
            {
                context.Entry(item).State = EntityState.Modified;
            }
            context.SaveChanges();
            //}
        }

        public void Remove(params T[] items)
        {
            //using (var context = new HRISSystemEntities())
            //{
            foreach (T item in items)
            {
                context.Entry(item).State = EntityState.Deleted;
            }
            context.SaveChanges();
            //}
        }
    }
}
