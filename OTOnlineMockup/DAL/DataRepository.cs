﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.DAL
{
    public interface IEmployeeRepository : IGenericDataRepository<Employee> { }
    public class EmployeeRepository : GenericDataRepository<Employee>, IEmployeeRepository { public EmployeeRepository(DbContext db) : base(db) { } }
    public interface IStaffStatuRepository : IGenericDataRepository<StaffStatu> { }
    public class StaffStatuRepository : GenericDataRepository<StaffStatu>, IStaffStatuRepository { public StaffStatuRepository(DbContext db) : base(db) { } }
    public interface IStaffTypeRepository : IGenericDataRepository<StaffType> { }
    public class StaffTypeRepository : GenericDataRepository<StaffType>, IStaffTypeRepository { public StaffTypeRepository(DbContext db) : base(db) { } }
    public interface IOTRepository : IGenericDataRepository<OT> { }
    public class OTRepository : GenericDataRepository<OT>, IOTRepository { public OTRepository(DbContext db) : base(db) { } }
    public interface IOTTypeRepository : IGenericDataRepository<OTType> { }
    public class OTTypeRepository : GenericDataRepository<OTType>, IOTTypeRepository { public OTTypeRepository(DbContext db) : base(db) { } }
    public interface IOTRequestTypeRepository : IGenericDataRepository<OTRequestType> { }
    public class OTRequestTypeRepository : GenericDataRepository<OTRequestType>, IOTRequestTypeRepository { public OTRequestTypeRepository(DbContext db) : base(db) { } }
    public interface IDepartmentRepository : IGenericDataRepository<Department> { }
    public class DepartmentRepository : GenericDataRepository<Department>, IDepartmentRepository { public DepartmentRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupAdminRepository : IGenericDataRepository<OTOnlineSetupAdmin> { }
    public class OTOnlineSetupAdminRepository : GenericDataRepository<OTOnlineSetupAdmin>, IOTOnlineSetupAdminRepository { public OTOnlineSetupAdminRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupAdminDetailRepository : IGenericDataRepository<OTOnlineSetupAdminDetail> { }
    public class OTOnlineSetupAdminDetailRepository : GenericDataRepository<OTOnlineSetupAdminDetail>, IOTOnlineSetupAdminDetailRepository { public OTOnlineSetupAdminDetailRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupApproverRepository : IGenericDataRepository<OTOnlineSetupApprover> { }
    public class OTOnlineSetupApproverRepository : GenericDataRepository<OTOnlineSetupApprover>, IOTOnlineSetupApproverRepository { public OTOnlineSetupApproverRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupApproverDetailRepository : IGenericDataRepository<OTOnlineSetupApproverDetail> { }
    public class OTOnlineSetupApproverDetailRepository : GenericDataRepository<OTOnlineSetupApproverDetail>, IOTOnlineSetupApproverDetailRepository { public OTOnlineSetupApproverDetailRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupFinalApproverRepository : IGenericDataRepository<OTOnlineSetupFinalApprover> { }
    public class OTOnlineSetupFinalApproverRepository : GenericDataRepository<OTOnlineSetupFinalApprover>, IOTOnlineSetupFinalApproverRepository { public OTOnlineSetupFinalApproverRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupFinalApproverDetailRepository : IGenericDataRepository<OTOnlineSetupFinalApproverDetail> { }
    public class OTOnlineSetupFinalApproverDetailRepository : GenericDataRepository<OTOnlineSetupFinalApproverDetail>, IOTOnlineSetupFinalApproverDetailRepository { public OTOnlineSetupFinalApproverDetailRepository(DbContext db) : base(db) { } }
    public interface IOTOnlineSetupHRApproverRepository : IGenericDataRepository<OTOnlineSetupHRApprover> { }
    public class OTOnlineSetupHRApproverRepository : GenericDataRepository<OTOnlineSetupHRApprover>, IOTOnlineSetupHRApproverRepository { public OTOnlineSetupHRApproverRepository(DbContext db) : base(db) { } }
    public interface ILot_NumberRepository : IGenericDataRepository<Lot_Number> { }
    public class Lot_NumberRepository : GenericDataRepository<Lot_Number>, ILot_NumberRepository { public Lot_NumberRepository(DbContext db) : base(db) { } }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
    //public interface IRepository: IGenericDataRepository<AA> { }
    //public class Repository : GenericDataRepository<AA>, IRepository {  public Repository(DbContext db) : base(db) {} }
}
