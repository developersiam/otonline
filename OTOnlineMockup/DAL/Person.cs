//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OTOnlineMockup.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Person
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Person()
        {
            this.Employees = new HashSet<Employee>();
        }
    
        public string Person_ID { get; set; }
        public string TitleName_ID { get; set; }
        public string FirstNameTH { get; set; }
        public string MidNameTH { get; set; }
        public string LastNameTH { get; set; }
        public string FirstNameEN { get; set; }
        public string MidNameEN { get; set; }
        public string LastNameEN { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public Nullable<decimal> Height { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public string Blood_ID { get; set; }
        public string Gender_ID { get; set; }
        public string MaritalStatus_ID { get; set; }
        public string Nationality_ID { get; set; }
        public string Race_ID { get; set; }
        public string Religion_ID { get; set; }
    
        public virtual TitleName TitleName { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employee> Employees { get; set; }
    }
}
