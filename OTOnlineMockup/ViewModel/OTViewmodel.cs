﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTOnlineMockup.DAL;

namespace OTOnlineMockup.ViewModel
{
    public class OTViewmodel : OT
    {
        public string EmployeeName { get; set; }
        public string AccountID { get; set; }
        public string FingerscanID { get; set; }
        public string OTTypeDetail { get; set; }
        public string RequestTypeDetail { get; set; }
        public string StatusName { get; set; }
        public string WholeStatus { get; set; }
        public bool AdminStatus { get; set; }
        public bool ManagerStatus { get; set; }
        public bool HrStatus { get; set; }
        public bool FinalApvrStatus { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public bool isReject { get; set; }
        public string strReject { get; set; }
    }
}
