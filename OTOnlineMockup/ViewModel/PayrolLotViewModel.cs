﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.ViewModel
{
    public class PayrolLotViewModel
    {
        public string payrollLot { get; set; }
        public DateTime startDate { get; set; }
        public DateTime finishDate { get; set; }
        public bool accountLocked { get; set; }
        public bool hrLocked { get; set; }
    }
}
