﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTOnlineMockup.DAL;

namespace OTOnlineMockup.ViewModel
{
    public class OTDetailViewModel : OT
    {
        public string TitleNameTH { get; set; }
        public string FirstNameTH { get; set; }
        public string LastNameTH { get; set; }
        public string FirstNameEN { get; set; }
        public string LastNameEN { get; set; }
    }
}
