﻿using OTOnlineMockup.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.ViewModel
{
    public class DepartmentViewModel : Dept_code
    {
        public string FullDeptNameTH { get; set; }
        public string FullDeptNameEN { get; set; }
    }
}
