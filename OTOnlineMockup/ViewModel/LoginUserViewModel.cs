﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.ViewModel
{
    public class LoginUserViewModel
    {
        public int? Admin_ID { get; set; }
        public int? Approver_ID { get; set; }
        public int? FinalApprover_ID { get; set; }
        public int? HRApprover_ID { get; set; }
        public string userName { get; set; }
        public string deptCode { get; set; }
        //public string userLevel { get; set; }
    }
}
