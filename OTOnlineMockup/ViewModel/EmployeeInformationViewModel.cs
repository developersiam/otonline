﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTOnlineMockup.DAL;

namespace OTOnlineMockup.ViewModel
{
    public class EmployeeInformationViewModel
    {
        public string EmployeeID { get; set; }
        public string AccountID { get; set; }
        public string FingerscanID { get; set; }
        public string NameTH { get; set; }
        public string NameEN { get; set; }
        public string StaffType { get; set; }
        public string Position { get; set; }
    }
}
