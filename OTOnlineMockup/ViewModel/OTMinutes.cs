﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.ViewModel
{
    public class OTMinutes
    {
        public string Minutes { get; set; }
        public decimal Multiplier { get; set; }
    }
}
