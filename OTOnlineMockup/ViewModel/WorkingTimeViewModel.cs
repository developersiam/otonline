﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.ViewModel
{
    public class WorkingTimeViewModel
    {
        public string checkInTime { get; set; }
        public string checkOutTime { get; set; }
        public string workingTime { get; set; }
    }
}
