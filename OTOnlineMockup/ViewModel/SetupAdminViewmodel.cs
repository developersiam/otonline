﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTOnlineMockup.DAL;

namespace OTOnlineMockup.ViewModel
{
    public class SetupAdminViewmodel : OTOnlineSetupAdmin
    {
        public string EmployeeName { get; set; }
        public string DeptID { get; set; }
        public string Dept_name { get; set; }
        public string ReportToName { get; set; }
        public string ReportToID { get; set; }
        public string Emails { get; set; }
    }
}
