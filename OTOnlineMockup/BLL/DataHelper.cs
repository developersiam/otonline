﻿using OTOnlineMockup.DAL;
using OTOnlineMockup.Model;
using OTOnlineMockup.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.BLL
{
    public interface IDataHelper
    {
        List<StaffStatu> getList_EmployeeStatus();
        List<StaffType> getList_StaffType();
        List<Employee> GetSearchedEmployee(string searchType, int? searchStaff, int? searchStatus, string searchText, string _requestFrom);
        List<DepartmentViewModel> getList_Department(string requestFrom);
        List<PayrolLotViewModel> getList_PayrollLot();
        List<OTViewmodel> getList_OT(string payrollLot, string requestFrom);
        List<Employee> getList_ApproveEmailDestination(List<OT> listOT, string requestFrom);
        OTDetailViewModel get_OTDetail(OTViewmodel model);
        List<Employee> getList_RejectEmailDestination(OT model, string requestFrom);
        List<OTType> getList_OTType();
        List<OTRequestType> getList_RequestType();
        LoginUserViewModel getCurentUser(string Username);
    }
    public class DataHelper : IDataHelper
    {
        public List<StaffStatu> getList_EmployeeStatus()
        {
            return DataAccessLayerServices.StaffStatuRepository().GetAll().ToList();
        }

        public List<StaffType> getList_StaffType()
        {
            return DataAccessLayerServices.StaffTypeRepository().GetAll().ToList();
        }

        public List<Employee> GetSearchedEmployee(string searchType, int? searchStaff, int? searchStatus, string searchText, string _requestFrom)
        {
            var employeeList = DataAccessLayerServices.EmployeeRepository().GetList(w => w.Staff_status == searchStatus,
                                                                                    w => w.Person,
                                                                                    w => w.StaffStatu,
                                                                                    w => w.StaffType).ToList(); //Filter employee by STATUS
            if (searchStaff != null) employeeList = employeeList.Where(w => w.Staff_type == searchStaff).ToList();

            if (!string.IsNullOrEmpty(searchText))
            {
                if (searchType == "ค้นหาจากชื่อ (TH)") employeeList = employeeList.Where(w => w.Person.FirstNameTH.Contains(searchText)).ToList();
                else if (searchType == "ค้นหาจากชื่อ (EN)") employeeList = employeeList.Where(w => w.Person.FirstNameEN.ToLower().Contains(searchText.ToLower())).ToList();
                else if (searchType == "ค้นหาจากนามสกุล (TH)") employeeList = employeeList.Where(w => w.Person.LastNameTH.Contains(searchText)).ToList();
                else if (searchType == "ค้นหาจากนามสกุล (EN)") employeeList = employeeList.Where(w => w.Person.LastNameEN.ToLower().Contains(searchText)).ToList();
                else if (searchType == "ค้นหาจากรหัสพนักงาน") employeeList = employeeList.Where(w => w.Employee_ID == searchText).ToList();
            }

            if (GlobalVariable.curentUser.HRApprover_ID.GetValueOrDefault() > 0 && _requestFrom == "HR Approver")
            {   // HR Can view all dept
                return employeeList;
            }
            else
            {   // Show only under reponse dept
                var listResult = from emp in employeeList
                                 join prm in GetList_Permission(_requestFrom) on emp.DeptCode equals prm.deptCode
                                 select emp;
                return listResult.Distinct().ToList();
            }
        }

        private List<LoginUserViewModel> GetList_Permission(string requestFrom)
        {
            List<LoginUserViewModel> listPermission = new List<LoginUserViewModel>();
            if (requestFrom == "Admin") listPermission = GetList_AdminPermission();
            else if (requestFrom == "Approver") listPermission = GetList_ApproverPermision();
            else if (requestFrom == "FinalApprover") listPermission = GetList_FinalApproverPermision();
            else if (requestFrom == "Report")
            {
                var adminPermission = GetList_AdminPermission();
                if (adminPermission != null && adminPermission.Any()) listPermission.AddRange(adminPermission);
                var approverPermission = GetList_ApproverPermision();
                if (approverPermission != null && approverPermission.Any()) listPermission.AddRange(approverPermission);
                var finalPermission = GetList_FinalApproverPermision();
                if (finalPermission != null && finalPermission.Any()) listPermission.AddRange(finalPermission);
            }
            return listPermission;
        }

        public List<LoginUserViewModel> GetList_AdminPermission()
        {
            var result = new List<LoginUserViewModel>();
            var _username = GlobalVariable.curentUser.userName.ToLower();
            var listAdmin = DataAccessLayerServices.OTOnlineSetupAdminDetailRepository().GetList(w => w.OTOnlineSetupAdmin.AdName.ToLower() == _username, w => w.OTOnlineSetupAdmin).ToList();
            result = (from adm in listAdmin select new LoginUserViewModel { Admin_ID = adm.Admin_ID, userName = adm.OTOnlineSetupAdmin.AdName, deptCode = adm.DeptID }).ToList();
            return result;
        }

        public List<LoginUserViewModel> GetList_ApproverPermision()
        {
            var result = new List<LoginUserViewModel>();
            var _username = GlobalVariable.curentUser.userName.ToLower();
            var listApprover = DataAccessLayerServices.OTOnlineSetupApproverDetailRepository().GetList(w => w.OTOnlineSetupApprover.StecUsername.ToLower() == _username, w => w.OTOnlineSetupApprover).ToList();
            result = (from apv in listApprover select new LoginUserViewModel { Admin_ID = apv.Approver_ID, userName = apv.OTOnlineSetupApprover.StecUsername, deptCode = apv.Dept_code }).ToList();
            return result;
        }

        public List<LoginUserViewModel> GetList_FinalApproverPermision()
        {
            var result = new List<LoginUserViewModel>();
            var _username = GlobalVariable.curentUser.userName.ToLower();
            var listFinalApprover = DataAccessLayerServices.OTOnlineSetupFinalApproverDetailRepository().GetList(w => w.OTOnlineSetupFinalApprover.StecUsername.ToLower() == _username, w => w.OTOnlineSetupFinalApprover).ToList();
            result = (from fpv in listFinalApprover select new LoginUserViewModel { Admin_ID = fpv.FinalApprover_ID, userName = fpv.OTOnlineSetupFinalApprover.StecUsername, deptCode = fpv.Dept_code }).ToList();
            return result;
        }

        public List<DepartmentViewModel> getList_Department(string requestFrom)
        {
            var listDepartment = DataAccessLayerServices.DepartmentRepository().GetAll().OrderBy(o => o.DeptName).ToList();
            var result =( from dep in listDepartment
                         select new DepartmentViewModel
                         {
                             Dept_code1 = dep.DeptCode,
                             Dept_name = dep.DeptName,
                             Dept_name_thai = dep.DeptNameTH,
                             FullDeptNameEN = string.Format("{0}: {1}", dep.DeptCode, dep.DeptName),
                             FullDeptNameTH = string.Format("{0}: {1}", dep.DeptCode, dep.DeptNameTH)
                         }).ToList();
            if (GlobalVariable.curentUser.HRApprover_ID.GetValueOrDefault() > 0 && (requestFrom == "HR Approver" || requestFrom == "Report"))
            {   // HR Approver Can view all dept (in HR approve and Report)
                return result;
            }
            else
            {   // Show only under reponse dept
                var listResult = (from dep in result
                                  join prm in GetList_Permission(requestFrom) on dep.Dept_code1 equals prm.deptCode
                                  select dep).ToList();
                return listResult;
            }
        }

        public List<PayrolLotViewModel> getList_PayrollLot()
        {
            var lot = DataAccessLayerServices.Lot_NumberRepository().GetAll().OrderByDescending(o => o.Lot_Year).ThenByDescending(o => o.Lot_Month).ToList();
            List<PayrolLotViewModel> result = new List<PayrolLotViewModel>();
            foreach (var item in lot)
            {
                result.Add(new PayrolLotViewModel
                {
                    payrollLot = item.Lot_Month + "/" + item.Lot_Year,
                    startDate = item.Start_date.GetValueOrDefault(),
                    finishDate = item.Finish_date.GetValueOrDefault(),
                    accountLocked = (item.Lock_Acc == "2" && item.Lock_Acc_Labor == "2"),
                    hrLocked = item.Lock_Hr_Labor == "2"
                });
            }
            return result.OrderByDescending(o => o.finishDate).ToList();
        }

        public List<OTViewmodel> getList_OT(string payrollLot, string requestFrom)
        {
            using (HRISSystemEntities dbHris = new HRISSystemEntities())
            {
                using (STEC_PayrollEntities pr = new STEC_PayrollEntities())
                {
                    List<OTViewmodel> result = new List<OTViewmodel>();
                    var listOT = dbHris.OTs.Where(w => w.PayrollLot != null && w.PayrollLot == payrollLot && w.Remark == "Create from OT Online").ToList();
                    var listEmployee = dbHris.Employees.ToList();
                    var listPerson = dbHris.People.ToList();
                    var listOTType = getList_OTType();
                    var listRequestType = getList_RequestType();
                    var listTitle = dbHris.TitleNames.ToList();
                    var listPEmployee = pr.Employee_Payroll.ToList();
                    var listDepertment = pr.Dept_code.ToList();

                    var listDocument = from ot in listOT
                                       join emp in listEmployee on ot.Employee_ID equals emp.Employee_ID
                                       join per in listPerson on emp.Person_ID equals per.Person_ID
                                       join tit in listTitle on per.TitleName_ID equals tit.TitleName_ID
                                       join pem in listPEmployee on emp.Noted equals pem.Employee_id
                                       join dep in listDepertment on pem.Dept_code equals dep.Dept_code1
                                       select new OTViewmodel
                                       {
                                           Employee_ID = emp.Employee_ID,
                                           AccountID = emp.Noted,
                                           FingerscanID = emp.FingerScanID,
                                           OTDate = ot.OTDate,
                                           OTType = ot.OTType,
                                           RequestType = ot.RequestType,
                                           NcountOT = ot.NcountOT,
                                           PayrollLot = ot.PayrollLot,
                                           Remark = ot.Remark,
                                           RemarkFromAdmin = ot.RemarkFromAdmin,
                                           RemarkFromManager = ot.RemarkFromManager,
                                           RemarkFromFinalApprover = ot.RemarkFromFinalApprover,
                                           RemarkFromHR = ot.RemarkFromHR,
                                           EmployeeName = per.FirstNameTH + " " + per.LastNameTH,
                                           OTTypeDetail = listOTType.SingleOrDefault(s => s.OTTypeCode == ot.OTType).OTTypeName,
                                           RequestTypeDetail = listRequestType.SingleOrDefault(s => s.RequestFlag == ot.RequestType).RequestFlagName,
                                           AdminApproveStatus = ot.AdminApproveStatus,
                                           AdminStatus = ot.AdminApproveStatus == "A",
                                           ManagerApproveStatus = ot.ManagerApproveStatus,
                                           ManagerStatus = ot.ManagerApproveStatus == "A",
                                           HRApproveStatus = ot.HRApproveStatus,
                                           HrStatus = ot.HRApproveStatus == "A",
                                           FinalApproverStatus = ot.FinalApproverStatus,
                                           FinalApvrStatus = ot.FinalApproverStatus == "A",
                                           DeptCode = pem.Dept_code,
                                           DeptName = dep.Dept_name_thai,
                                           isReject = (ot.ManagerApproveStatus == "R" || ot.FinalApproverStatus == "R" || ot.HRApproveStatus == "R"),
                                           strReject = (ot.ManagerApproveStatus == "R" || ot.FinalApproverStatus == "R" || ot.HRApproveStatus == "R") ? "Reject" : ""
                                       };
                    //if (GlobalVariable.userDepartment == "IT" || GlobalVariable.curentUser.FirstOrDefault().userLevel == "HR Approver")
                    if (GlobalVariable.curentUser.HRApprover_ID.GetValueOrDefault() > 0 && (requestFrom == "HR Approver" || requestFrom == "Report"))
                    {   // HR Approver Can view all dept
                        return listDocument.OrderBy(o => o.OTDate).ThenBy(t => t.EmployeeName).ToList();
                    }
                    else
                    {   // Show only under reponse dept                        
                        var listResult = from doc in listDocument
                                         join prm in GetList_Permission(requestFrom) on doc.DeptCode equals prm.deptCode
                                         select doc;
                        return listResult.Distinct().OrderBy(o => o.OTDate).ThenBy(t => t.EmployeeName).ToList();
                    }
                }
            }
        }

        public List<Employee> getList_ApproveEmailDestination(List<OT> listOT, string requestFrom)
        {
            try
            {
                using (HRISSystemEntities hris = new HRISSystemEntities())
                {
                    using (STEC_PayrollEntities proll = new STEC_PayrollEntities())
                    {
                        // Get Group of OT Department
                        var listEmployee = hris.Employees.ToList();
                        var listPEmployee = proll.Employee_Payroll.ToList();
                        var listDept = (from emp in listEmployee
                                        join pmp in listPEmployee on emp.Noted equals pmp.Employee_id
                                        join ots in listOT on emp.Employee_ID equals ots.Employee_ID
                                        select new Dept_code
                                        { Dept_code1 = pmp.Dept_code }).ToList();
                        var listGroupDept = (from dep in listDept
                                             group dep by dep.Dept_code1 into deptGroup
                                             orderby deptGroup.Key ascending
                                             select new Dept_code
                                             { Dept_code1 = deptGroup.Key }).ToList();

                        //Get Destination Email
                        List<Employee> listEmail = new List<Employee>();
                        if (requestFrom == "Admin")
                        {   // Send to Approver
                            var listApprover = hris.OTOnlineSetupApprovers.ToList();
                            var listApproveDet = hris.OTOnlineSetupApproverDetails.ToList();
                            listEmail = (from apv in listApprover
                                         join apd in listApproveDet on apv.Approver_ID equals apd.Approver_ID
                                         join dep in listGroupDept on apd.Dept_code equals dep.Dept_code1
                                         select new Employee { Email = apv.Email }).ToList();
                        }
                        else if (requestFrom == "Approver")
                        {   // Send to Final Approver
                            var listFApprover = hris.OTOnlineSetupFinalApprovers.ToList();
                            var listFApproveDet = hris.OTOnlineSetupFinalApproverDetails.ToList();
                            listEmail = (from fav in listFApprover
                                         join fad in listFApproveDet on fav.FinalApprover_ID equals fad.FinalApprover_ID
                                         join dep in listGroupDept on fad.Dept_code equals dep.Dept_code1
                                         select new Employee { Email = fav.Email }).ToList();
                        }
                        else if (requestFrom == "FinalApprover")
                        {   // Send to HR Approver
                            var listHRApprover = hris.OTOnlineSetupApprovers.ToList();
                            listEmail = (from hav in listHRApprover
                                         select new Employee { Email = hav.Email }).ToList();
                        }

                        var listGroupEmail = (from eml in listEmail
                                              group eml by eml.Email into emailGroup
                                              select new Employee { Email = emailGroup.Key }).ToList();
                        return listGroupEmail;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OTDetailViewModel get_OTDetail(OTViewmodel model)
        {
            using (HRISSystemEntities dbHris = new HRISSystemEntities())
            {
                var listOT = dbHris.OTs.Where(w => w.Employee_ID == model.Employee_ID &&
                                                   w.OTDate == model.OTDate &&
                                                   w.OTType == model.OTType &&
                                                   w.RequestType == model.RequestType).ToList();
                var listEmployee = dbHris.Employees.Where(f => f.Employee_ID == model.Employee_ID).ToList();
                var listPerson = dbHris.People.ToList();
                var listTitle = dbHris.TitleNames.ToList();
                var listOTDetail = from ot in listOT
                                   join emp in listEmployee on ot.Employee_ID equals emp.Employee_ID
                                   join per in listPerson on emp.Person_ID equals per.Person_ID
                                   join tit in listTitle on per.TitleName_ID equals tit.TitleName_ID
                                   select new OTDetailViewModel
                                   {
                                       Employee_ID = emp.Employee_ID,
                                       OTDate = ot.OTDate,
                                       OTType = ot.OTType,
                                       RequestType = ot.RequestType,
                                       NcountOT = ot.NcountOT,
                                       PayrollLot = ot.PayrollLot,
                                       Remark = ot.Remark,
                                       TitleNameTH = tit.TitleNameTH,
                                       FirstNameTH = per.FirstNameTH,
                                       LastNameTH = per.LastNameTH,
                                       FirstNameEN = per.FirstNameEN,
                                       LastNameEN = per.LastNameEN,
                                       RemarkFromAdmin = ot.RemarkFromAdmin,
                                       RemarkFromManager = ot.RemarkFromManager,
                                       RemarkFromFinalApprover = ot.RemarkFromFinalApprover,
                                       RemarkFromHR = ot.RemarkFromHR
                                   };
                return listOTDetail.FirstOrDefault();
            }
        }

        public List<Employee> getList_RejectEmailDestination(OT model, string requestFrom)
        {
            try
            {
                using (HRISSystemEntities hris = new HRISSystemEntities())
                {
                    using (STEC_PayrollEntities proll = new STEC_PayrollEntities())
                    {
                        // Get Group of OT Department
                        var listEmployee = hris.Employees.Where(w => w.Employee_ID == model.Employee_ID).ToList();
                        var listPEmployee = proll.Employee_Payroll.ToList();
                        var listDept = (from emp in listEmployee
                                        join pmp in listPEmployee on emp.Noted equals pmp.Employee_id
                                        select new Dept_code { Dept_code1 = pmp.Dept_code }).ToList();
                        string employeeDept = listDept.FirstOrDefault().Dept_code1;
                        //Get Destination Email
                        List<Employee> listEmail = new List<Employee>();

                        // Send to Admin
                        var listAdmin = hris.OTOnlineSetupAdmins.ToList();
                        var listAdminDet = hris.OTOnlineSetupAdminDetails.Where(w => w.DeptID == employeeDept).ToList();
                        var listAdminEmail = (from adm in listAdmin
                                              join adt in listAdminDet on adm.Admin_ID equals adt.Admin_ID
                                              join emp in hris.Employees on adm.Employee_ID equals emp.Employee_ID
                                              select new Employee { Email = emp.Email }).ToList();
                        listEmail.AddRange(listAdminEmail);

                        if (requestFrom == "FinalApprover" || requestFrom == "HR Approver")
                        {
                            // Send to Approver
                            var listApprover = hris.OTOnlineSetupApprovers.ToList();
                            var listApproveDet = hris.OTOnlineSetupApproverDetails.Where(w => w.Dept_code == employeeDept).ToList();
                            var listApproverEmail = (from apv in listApprover
                                                     join apd in listApproveDet on apv.Approver_ID equals apd.Approver_ID
                                                     select new Employee { Email = apv.Email }).ToList();
                            listEmail.AddRange(listApproverEmail);
                        }
                        if (requestFrom == "HR Approver")
                        {   // Send to Final Approver
                            var listFApprover = hris.OTOnlineSetupFinalApprovers.ToList();
                            var listFApproveDet = hris.OTOnlineSetupFinalApproverDetails.Where(w => w.Dept_code == employeeDept).ToList();
                            var listFinalApproverEmail = (from fav in listFApprover
                                                          join fad in listFApproveDet on fav.FinalApprover_ID equals fad.FinalApprover_ID
                                                          select new Employee { Email = fav.Email }).ToList();
                            listEmail.AddRange(listFinalApproverEmail);
                        }

                        var listGroupEmail = (from eml in listEmail
                                              group eml by eml.Email into emailGroup
                                              select new Employee { Email = emailGroup.Key }).ToList();
                        return listGroupEmail;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OTType> getList_OTType()
        {
            return DataAccessLayerServices.OTTypeRepository().GetAll().ToList();
        }

        public List<OTRequestType> getList_RequestType()
        {
            return DataAccessLayerServices.OTRequestTypeRepository().GetAll().ToList();
        }

        public LoginUserViewModel getCurentUser(string Username)
        {
            var adm = DataAccessLayerServices.OTOnlineSetupAdminRepository().GetSingle(w => w.AdName == Username);
            var apv = DataAccessLayerServices.OTOnlineSetupApproverRepository().GetSingle(w => w.StecUsername == Username);
            var fav = DataAccessLayerServices.OTOnlineSetupFinalApproverRepository().GetSingle(w => w.StecUsername == Username);
            var hav = DataAccessLayerServices.OTOnlineSetupHRApproverRepository().GetSingle(w => w.StecUsername == Username);
            return new LoginUserViewModel
            {
                userName = Username,
                Admin_ID = adm == null ? 0 : adm.Admin_ID,
                Approver_ID = apv == null ? 0 : apv.Approver_ID,
                FinalApprover_ID = fav == null ? 0 : fav.FinalApprover_ID,
                HRApprover_ID = hav == null ? 0 : hav.HRApprover_ID
            };
        }

        //string _userName;
        //string _userLevel;
        //int _adminID;

        public List<LoginUserViewModel> GetList_HRPermision()
        {
            try
            {
                using (HRISSystemEntities hris = new HRISSystemEntities())
                {
                    var listHRApprover = hris.OTOnlineSetupHRApprovers.Where(w => w.StecUsername.ToLower() == GlobalVariable.curentUser.userName.ToLower()).ToList();

                    var listResult = from hra in listHRApprover
                                     select new LoginUserViewModel
                                     {
                                         HRApprover_ID = hra.HRApprover_ID,
                                         userName = hra.StecUsername
                                         //userLevel = "HR Approver"
                                     };
                    return listResult.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> get_DocumentStatus()
        {
            List<string> result = new List<string>();
            result.Add("ทั้งหมด");
            result.Add("อนุมัติแล้ว (Approved)");
            result.Add("ยังไม่ได้อนุมัติ (Un Approve..)");
            result.Add("รออนุมัติจาก Manager (Manager Pending)");
            result.Add("รออนุมัติจาก HR (HR Pending)");
            result.Add("ปฏิเสธ (Reject)");
            return result;
        }

        

        private string titleName(string TitleName_ID)
        {
            if (TitleName_ID == "1") return "นาย";
            else if (TitleName_ID == "2") return "นาง";
            else return "นางสาว";
        }

        


        public List<SetupAdminViewmodel> getList_Admin()
        {
            using (HRISSystemEntities dbHris = new HRISSystemEntities())
            {
                using (STEC_PayrollEntities pr = new STEC_PayrollEntities())
                {
                    List<SetupAdminViewmodel> result = new List<SetupAdminViewmodel>();
                    var listAdmin = dbHris.OTOnlineSetupAdmins.ToList();
                    var listDetail = dbHris.OTOnlineSetupAdminDetails.ToList();
                    var listEmployee = dbHris.Employees.ToList();
                    var listPerson = dbHris.People.ToList();
                    var listDept = pr.Dept_code.ToList();

                    var listResult = from adm in listAdmin
                                     join adt in listDetail on adm.Admin_ID equals adt.Admin_ID
                                     join emp in listEmployee on adm.Employee_ID equals emp.Employee_ID
                                     join per in listPerson on emp.Person_ID equals per.Person_ID
                                     join dep in listDept on adt.DeptID equals dep.Dept_code1
                                     select new SetupAdminViewmodel
                                     {
                                         Admin_ID = adm.Admin_ID,
                                         Employee_ID = adm.Employee_ID,
                                         AdName = adm.AdName,
                                         EmployeeName = per.FirstNameTH + " " + per.LastNameTH,
                                         DeptID = adt.DeptID,
                                         Dept_name = adt.DeptID + ": " + dep.Dept_name,
                                         //ReportToID = adt.ReportTo,
                                         //ReportToName = reportTo(listEmployee, listPerson, adt.ReportTo),
                                         ValidFrom = adt.ValidFrom,
                                         EndDate = adt.EndDate,
                                         Emails = emp.Email
                                     };
                    return listResult.OrderBy(o => o.EmployeeName).ToList();
                }
            }
        }

        //private string reportTo(List<Employee> listEmployee, List<Person> listPerson, string ReportTo)
        //{
        //    var personID = listEmployee.SingleOrDefault(e => e.Employee_ID == ReportTo).Person_ID;
        //    var person = listPerson.SingleOrDefault(s => s.Person_ID == personID);
        //    return person.FirstNameTH + " " + person.LastNameTH;
        //}




        public string get_EmailByEmployeeID(string Employee_ID)
        {
            using (HRISSystemEntities hris = new HRISSystemEntities())
            {
                var emp = hris.Employees.SingleOrDefault(s => s.Employee_ID == Employee_ID);
                string email = emp != null ? emp.Email : "";
                return email;
            }
        }

        public List<string> getList_SearchEmployeeType()
        {
            List<string> result = new List<string>();
            result.Add("ค้นหาจากชื่อ (TH)");
            result.Add("ค้นหาจากชื่อ (EN)");
            result.Add("ค้นหาจากนามสกุล (TH)");
            result.Add("ค้นหาจากนามสกุล (EN)");
            result.Add("ค้นหาจากรหัส HR");
            result.Add("ค้นหาจากรหัส Payroll");
            result.Add("ค้นหาจากรหัสบัตรประชาชน");
            return result;
        }

        public List<EmployeeInformationViewModel> convertToSearchedEmployee(List<sp_OT_GetEmployeeInformation_Result> listFiltered)
        {
            List<EmployeeInformationViewModel> result = new List<EmployeeInformationViewModel>();
            foreach (var item in listFiltered)
            {
                result.Add(new EmployeeInformationViewModel
                {
                    EmployeeID = item.Employee_ID,
                    AccountID = item.ACC_code,
                    FingerscanID = item.FingerScanID,
                    NameTH = item.TitleNameTH + " " + item.FirstNameTH + " " + item.LastNameTH,
                    NameEN = item.FirstNameEN + " " + item.LastNameEN,
                    StaffType = item.Staff_type,
                    Position = item.PositionTH
                });
            }
            return result;
        }

        

        public bool AddAdminDetail(string adminEmployeeID, string adName, OTOnlineSetupAdminDetail adminDetail)
        {
            try
            {
                using (HRISSystemEntities dbHris = new HRISSystemEntities())
                {
                    int adminID;
                    //DateTime endDate = Convert.ToDateTime("9999-12-31");
                    var setupAdmin = dbHris.OTOnlineSetupAdmins.SingleOrDefault(s => s.Employee_ID == adminEmployeeID);
                    if (setupAdmin == null)
                    {
                        dbHris.OTOnlineSetupAdmins.Add(new OTOnlineSetupAdmin
                        {
                            Employee_ID = adminEmployeeID,
                            AdName = adName,
                            CreateDate = DateTime.Now,
                            ValidFrom = DateTime.Now,
                            EndDate = adminDetail.EndDate
                        });
                        dbHris.SaveChanges();
                        adminID = dbHris.OTOnlineSetupAdmins.SingleOrDefault(s => s.Employee_ID == adminEmployeeID).Admin_ID;
                    }
                    else
                    {
                        adminID = setupAdmin.Admin_ID;
                    }

                    dbHris.OTOnlineSetupAdminDetails.Add(new OTOnlineSetupAdminDetail
                    {
                        Admin_ID = adminID,
                        DeptID = adminDetail.DeptID,
                        ReportTo = adminDetail.ReportTo,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        ValidFrom = DateTime.Now,
                        EndDate = adminDetail.EndDate
                    });
                    dbHris.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteAdminDetail(OTOnlineSetupAdminDetail adminDetail)
        {
            try
            {
                using (HRISSystemEntities dbHris = new HRISSystemEntities())
                {
                    var model = dbHris.OTOnlineSetupAdminDetails.SingleOrDefault(s => s.Admin_ID == adminDetail.Admin_ID && s.DeptID == adminDetail.DeptID);
                    if (model == null) throw new ArgumentNullException("Error, data not found.");
                    dbHris.OTOnlineSetupAdminDetails.Remove(model);
                    dbHris.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public WorkingTimeViewModel get_WorkingTime(DateTime selectedDate, Employee employeeModel)
        {
            WorkingTimeViewModel result = new WorkingTimeViewModel();
            var _checkInOut = get_CheckInOut(selectedDate, employeeModel.FingerScanID);
            var _manualDate = get_ManualWorkDate(selectedDate, employeeModel.Employee_ID);
            if (_checkInOut.Count() <= 0 && _manualDate.Count() <= 0) return null;
            TimeSpan? timeIn, timeOut;
            string strTotalTime = "";
            //get TimeIn
            if (_checkInOut.Count > 0) timeIn = _checkInOut.Min(c => c.checktime.TimeOfDay);
            else if (_manualDate.Where(e => e.ManualWorkDateType_ID == "A01").Count() > 0) timeIn = _manualDate.Where(e => e.ManualWorkDateType_ID == "A01").FirstOrDefault().Times;
            else timeIn = null;
            //get TimeOut
            if (_manualDate.Where(e => e.ManualWorkDateType_ID == "A02").Count() > 0) timeOut = _manualDate.Where(e => e.ManualWorkDateType_ID == "A02").FirstOrDefault().Times;
            else if (_checkInOut.Count > 0) timeOut = _checkInOut.Max(c => c.checktime.TimeOfDay);
            else timeOut = null;
            //get Total
            if (timeIn != null && timeOut != null)
            {
                TimeSpan timeTotal = timeOut.GetValueOrDefault() - timeIn.GetValueOrDefault();
                timeTotal = (timeIn.GetValueOrDefault() != timeOut.GetValueOrDefault()) ? timeTotal.Subtract(new TimeSpan(0, 30, 0)) : timeTotal; // If timein <> timeout the Subtract 30 minutes for Break.
                strTotalTime = string.Format("{0} hours , {1} minutes", timeTotal.Hours, timeTotal.Minutes);
            }
            return new WorkingTimeViewModel { checkInTime = timeIn.ToString(), checkOutTime = timeOut.ToString(), workingTime = strTotalTime };
        }

        private List<checkinout> get_CheckInOut(DateTime selectedDate, string fingerscanID)
        {
            using (TimeSTECEntities dbTime = new TimeSTECEntities())
            {
                int _fingerScan = Convert.ToInt32(fingerscanID);
                var _userInfo = dbTime.sp_Get_UserInfobyFingerId(_fingerScan).SingleOrDefault();
                return dbTime.checkinouts.Where(w => w.userid == _userInfo.userid && DbFunctions.TruncateTime(w.checktime) == selectedDate && (w.sn_name != "BXUO190360026" && w.sn_name != "BXUO190360022")).ToList();
            }
        }

        private List<ManualWorkDate> get_ManualWorkDate(DateTime selectedDate, string employeeID)
        {
            using (HRISSystemEntities dbHris = new HRISSystemEntities())
            {
                return dbHris.ManualWorkDates.Where(w => w.Employee_ID == employeeID && DbFunctions.TruncateTime(w.ManualWorkDateDate) == selectedDate).ToList();
            }
        }

        public Holiday get_Holiday(DateTime selectedDate)
        {
            using (HRISSystemEntities dbHris = new HRISSystemEntities())
            {
                return dbHris.Holidays.SingleOrDefault(s => s.Crop == selectedDate.Year && DbFunctions.TruncateTime(s.Date) == selectedDate);
            }
        }

        public bool AddOTDetail(OT model)
        {
            try
            {
                using (HRISSystemEntities dbHris = new HRISSystemEntities())
                {
                    if (model == null) throw new ArgumentException("ไม่สามารถบันทึกข้อมูลได้ กรุณาติดต่อแผนก IT");
                    if (model.OTDate == null) throw new ArgumentException("กรุณาตรวจสอบข้อมูล OT date");
                    if (string.IsNullOrEmpty(model.Employee_ID)) throw new ArgumentException("กรุณาตรวจสอบข้อมูล Employee_ID");
                    if (model.NcountOT.GetValueOrDefault() <= 0) throw new ArgumentException("กรุณาตรวจสอบข้อมูลจำนวน OT");
                    //Check duplicate.
                    var ot = dbHris.OTs.SingleOrDefault(s => s.Employee_ID == model.Employee_ID &&
                                                             s.OTDate == model.OTDate &&
                                                             s.OTType == model.OTType &&
                                                             s.RequestType == model.RequestType);
                    if (ot != null) throw new ArgumentException("ไม่สามารถบันทึกข้อมูลซ้ำได้ ข้อมูลนี้มีอยู่ในระบบแล้ว");
                    model.ModifiedUser = GlobalVariable.curentUser.userName;
                    model.Admin_ID = GlobalVariable.curentUser.Admin_ID;
                    dbHris.OTs.Add(model);
                    dbHris.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateOTDetail(OT model, string actionFrom, string formState)
        {
            try
            {
                using (HRISSystemEntities dbHris = new HRISSystemEntities())
                {
                    if (model == null) throw new ArgumentException("ไม่สามารถบันทึกข้อมูลได้ กรุณาติดต่อแผนก IT");
                    if (model.OTDate == null) throw new ArgumentException("กรุณาตรวจสอบข้อมูล OT date");
                    if (string.IsNullOrEmpty(model.Employee_ID)) throw new ArgumentException("กรุณาตรวจสอบข้อมูล Employee_ID");
                    if (formState == "Update" && model.NcountOT.GetValueOrDefault() <= 0) throw new ArgumentException("กรุณาตรวจสอบข้อมูลจำนวน OT");
                    //Check is data avialable.
                    var ot = dbHris.OTs.SingleOrDefault(s => s.Employee_ID == model.Employee_ID &&
                                                             s.OTDate == model.OTDate &&
                                                             s.OTType == model.OTType &&
                                                             s.RequestType == model.RequestType);
                    if (ot == null) throw new ArgumentException("ไม่พบข้อมูลโอทีนี้หรือข้อมูลอาจถูกลบจากระบบ กรุณาตรวจสอบข้อมูลอีกครั้ง");
                    else
                    {
                        if (actionFrom == "Admin")
                        {   // Admin Approve / Update
                            if (formState == "Approve")
                            {
                                ot.AdminApproveStatus = "A";
                                ot.ManagerApproveStatus = null;
                                ot.HRApproveStatus = null;
                                ot.FinalApproverStatus = null;
                            }
                            else if (formState == "Update")
                            {
                                //if (!string.IsNullOrEmpty(ot.ManagerApproveStatus)) throw new ArgumentException("ไม่สามารถอัพเดทข้อมูลโอทีนี้ได้ กรุณา refresh ข้อมูลอีกครั้ง");
                                ot.NcountOT = model.NcountOT;
                                ot.RemarkFromAdmin = model.RemarkFromAdmin;
                            }
                            ot.ModifiedUser = GlobalVariable.curentUser.userName;
                        }
                        else if (actionFrom == "Approver")
                        {   // Manager Approve / Reject (Approver)
                            if (formState == "Approve") ot.ManagerApproveStatus = "A";
                            else if (formState == "Reject")
                            {
                                ot.AdminApproveStatus = null;
                                ot.ManagerApproveStatus = "R";
                                ot.RemarkFromManager = model.RemarkFromManager;
                            }
                            ot.ManagerApproveUser = GlobalVariable.curentUser.userName;
                        }
                        else if (actionFrom == "FinalApprover")
                        {   // Factory Manager Approve (Final Approver)
                            if (formState == "Approve") ot.FinalApproverStatus = "A";
                            else if (formState == "Reject")
                            {
                                ot.AdminApproveStatus = null;
                                ot.ManagerApproveStatus = null;
                                ot.FinalApproverStatus = "R";
                                ot.RemarkFromFinalApprover = model.RemarkFromFinalApprover;
                            }
                            ot.FinalApproverUser = GlobalVariable.curentUser.userName;
                        }
                        else if (actionFrom == "HR")
                        {   // HR Approve / Reject
                            if (formState == "Approve") { ot.HRApproveStatus = "A"; ot.Status = true; }
                            else if (formState == "Reject")
                            {
                                ot.AdminApproveStatus = null;
                                ot.ManagerApproveStatus = null;
                                ot.FinalApproverStatus = null;
                                ot.HRApproveStatus = "R";
                                ot.RemarkFromHR = model.RemarkFromHR;
                            }
                            ot.HrApproveUser = GlobalVariable.curentUser.userName;
                        }
                        ot.ModifiedDate = DateTime.Now;
                        dbHris.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteOTDetail(OT model)
        {
            using (HRISSystemEntities dbHris = new HRISSystemEntities())
            {
                if (model == null) throw new ArgumentException("ไม่สามารถบันทึกข้อมูลได้ กรุณาติดต่อแผนก IT");
                if (model.OTDate == null) throw new ArgumentException("กรุณาตรวจสอบข้อมูล OT date");
                if (string.IsNullOrEmpty(model.Employee_ID)) throw new ArgumentException("กรุณาตรวจสอบข้อมูล Employee_ID");
                //Check Existing.
                var ot = dbHris.OTs.SingleOrDefault(s => s.Employee_ID == model.Employee_ID &&
                                                         s.OTDate == model.OTDate &&
                                                         s.OTType == model.OTType &&
                                                         s.RequestType == model.RequestType);
                if (ot == null) throw new ArgumentException("ไม่สามารถลบข้อมูลได้ ไม่พบข้อมูลนี้ในระบบ");
                dbHris.OTs.Remove(ot);
                dbHris.SaveChanges();
            }
            return true;
        }
    }
}
