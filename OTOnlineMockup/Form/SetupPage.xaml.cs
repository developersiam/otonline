﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OTOnlineMockup.DAL;
using OTOnlineMockup.ViewModel;
using OTOnlineMockup.BLL;
using OTOnlineMockup.Model;

namespace OTOnlineMockup.Form
{
    /// <summary>
    /// Interaction logic for SetupPage.xaml
    /// </summary>
    public partial class SetupPage : Page
    {
        DataHelper dt;
        Employee _searchedEmployee;
        string _reportTo;

        public SetupPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            dt = new DataHelper();
            DeptCombobox.ItemsSource = dt.getList_Department("HR Approver");
            ValidToDateDatePicker.DisplayDateStart = DateTime.Now.AddDays(1);
            ValidToDateDatePicker.SelectedDate = Convert.ToDateTime("9999-12-31");
            ReloadDatagrid();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            AdminDataGrid.ItemsSource = null;
            var adminlist = dt.getList_Admin();
            if (!string.IsNullOrEmpty(EmployeeIDTextBox.Text)) adminlist = adminlist.Where(w => w.Employee_ID == EmployeeIDTextBox.Text).ToList();
            AdminDataGrid.ItemsSource = dt.getList_Admin();
        }

        private void EmployeeIDTextBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Search("Employee");
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search("Employee");
        }
        

        private void Search(string searchType)
        {
            SearchEmployee_Window window = new SearchEmployee_Window("HR Approver");
            window.ShowDialog();
            if (window.selectedEmployee != null)
            {
                _searchedEmployee = window.selectedEmployee;
                if (searchType == "Employee")
                {
                    EmployeeIDTextBox.Text = _searchedEmployee.Employee_ID;
                    NameTextBox.Text = _searchedEmployee.Person.FirstNameTH + " " + _searchedEmployee.Person.LastNameTH;
                    string email = dt.get_EmailByEmployeeID(_searchedEmployee.Employee_ID);
                    EmailTextBox.Text = email;
                    SaveButton.IsEnabled = !string.IsNullOrEmpty(email);
                    if (string.IsNullOrEmpty(email)) MessageBox.Show("ผู้ใช้ที่เลือกยังไม่ได้ระบุ Email ในฐานข้อมูล HRIS", "ไม่สามารถบันทึกได้", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    _reportTo = _searchedEmployee.Employee_ID;
                    ReportToTextBox.Text = _searchedEmployee.Person.FirstNameTH + " " + _searchedEmployee.Person.LastNameTH; ;
                }
                //GlobalVariable.searchedEmployee = null;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(EmployeeIDTextBox.Text) || string.IsNullOrEmpty(NameTextBox.Text) || string.IsNullOrEmpty(AdNameTextBox.Text) || string.IsNullOrEmpty(EmailTextBox.Text))
                {
                    MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;

                OTOnlineSetupAdminDetail adminDetail = new OTOnlineSetupAdminDetail
                {
                    DeptID = (string)DeptCombobox.SelectedValue,
                    EndDate = ValidToDateDatePicker.SelectedDate
                };

                if (dt.AddAdminDetail(EmployeeIDTextBox.Text, AdNameTextBox.Text, adminDetail)) MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Information");
                Clear();
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            _reportTo = string.Empty;
            EmployeeIDTextBox.Text = string.Empty;
            NameTextBox.Text = string.Empty;
            EmailTextBox.Text = string.Empty;
            DeptCombobox.Text = string.Empty;
            ValidToDateDatePicker.SelectedDate = Convert.ToDateTime("9999-12-31");
            SaveButton.IsEnabled = false;
            //ReportToTextBox.Text = string.Empty;
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                var selectedItem = (SetupAdminViewmodel)AdminDataGrid.SelectedItem;
                OTOnlineSetupAdminDetail model = new OTOnlineSetupAdminDetail { Admin_ID = selectedItem.Admin_ID, DeptID = selectedItem.DeptID };
                if (dt.DeleteAdminDetail(model)) MessageBox.Show("ลบข้อมูลสำเร็จ", "Information");
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

    }
}
