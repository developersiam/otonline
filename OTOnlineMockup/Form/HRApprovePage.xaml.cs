﻿using OTOnlineMockup.BLL;
using OTOnlineMockup.DAL;
using OTOnlineMockup.Model;
using OTOnlineMockup.Service;
using OTOnlineMockup.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OTOnlineMockup.Form
{
    /// <summary>
    /// Interaction logic for HRApprovePage.xaml
    /// </summary>
    public partial class HRApprovePage : Page
    {
        DataHelper dt;
        Employee _searchedEmployee;
        List<OTViewmodel> listFilter = new List<OTViewmodel>();
        int havID = 0;
        public HRApprovePage()
        {
            InitializeComponent();
            dt = new DataHelper();
            havID = GlobalVariable.curentUser.HRApprover_ID.GetValueOrDefault();
            if (havID > 0)
            {
                BindPayrollLot();
                //DeptCombobox.ItemsSource = dt.getList_Department("HR Approver").OrderBy(o => o.Dept_code1);
            }
            else DisableUnpermit();
        }

        private void DisableUnpermit()
        {
            NameTextBox.IsEnabled = false;
            SearchButton.IsEnabled = false;
            ClearButton.IsEnabled = false;
            LotToDateDatePicker.IsEnabled = false;
            LotFromDateDatePicker.IsEnabled = false;
            DeptCombobox.IsEnabled = false;
            RefreshButton.IsEnabled = false;
        }

        private void BindPayrollLot()
        {
            var payrollLot = dt.getList_PayrollLot();
            LotCombobox.ItemsSource = payrollLot;
            LotCombobox.SelectedValue = "06/2018";
            //LotCombobox.SelectedValue = DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year;
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                OTDataGrid.ItemsSource = null;
                var selectedLot = (PayrolLotViewModel)LotCombobox.SelectedItem;
                DateTime displayDateStart = selectedLot.startDate;
                DateTime displayDateEnd = selectedLot.finishDate;
                LotFromDateDatePicker.DisplayDateStart = displayDateStart;
                LotFromDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotFromDateDatePicker.SelectedDate = displayDateStart;
                LotToDateDatePicker.DisplayDateStart = displayDateStart;
                LotToDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotToDateDatePicker.SelectedDate = displayDateEnd;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void NameTextBox_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            if (havID > 0)
            {
                SearchEmployee_Window window = new SearchEmployee_Window("HR Approver");
                window.ShowDialog();
                if (window.selectedEmployee != null)
                {
                    _searchedEmployee = window.selectedEmployee;
                    NameTextBox.Text = _searchedEmployee.Person.FirstNameTH + " " + _searchedEmployee.Person.LastNameTH;
                    ReloadDataGrid();
                }
            }
            else MessageBox.Show("ชื่อผู้ใช้นี้ไม่ได้รับสิทธิให้ใช้งานสำหรับส่วนนี้", "คำเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void ReloadDataGrid()
        {
            if (havID > 0)
            {

                OTDataGrid.ItemsSource = null;
                var listOTData = dt.getList_OT(LotCombobox.Text, "HR Approver");
                var listNameFilter = _searchedEmployee == null ? listOTData :
                                                                 listOTData.Where(w => w.Employee_ID == _searchedEmployee.Employee_ID).ToList();
                var listDeptFilter = DeptCombobox.Text == "" ? listNameFilter :
                                                               listNameFilter.Where(w => w.DeptCode == DeptCombobox.SelectedValue.ToString()).ToList();
                listFilter = listDeptFilter.Where(w => w.AdminApproveStatus == "A" &&
                                                       w.ManagerApproveStatus == "A" &&
                                                       w.FinalApproverStatus == "A" &&
                                                       w.HRApproveStatus == null &&
                                                       w.OTDate >= LotFromDateDatePicker.SelectedDate &&
                                                       w.OTDate <= LotToDateDatePicker.SelectedDate).ToList();
                OTDataGrid.ItemsSource = listFilter;
                TotalDocumentTextBox.Text = listFilter.Count().ToString("N0");
            }
            else MessageBox.Show("ชื่อผู้ใช้นี้ไม่ได้รับสิทธิให้ใช้งานสำหรับส่วนนี้", "คำเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void ViewButton_Click(object sender, RoutedEventArgs e)
        {
            OTRequestDetailWindow window = new OTRequestDetailWindow((OTViewmodel)OTDataGrid.SelectedItem, "HR", "View");
            window.ShowDialog();
            ReloadDataGrid();
        }

        private void RejectButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(havID > 0))
            {
                MessageBox.Show("ไม่สามารถบันทึก/แก้ไขข้อมูลได้ ผู้ใช้นี้ไม่ได้มีสิทธิ์เป็น HR Approver", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            OTRequestDetailWindow window = new OTRequestDetailWindow((OTViewmodel)OTDataGrid.SelectedItem, "HR", "Reject");
            window.ShowDialog();
            ReloadDataGrid();
        }

        private void DatagridCheckBox_Click(object sender, RoutedEventArgs e)
        {
            //CheckBox dtCheckbox = (CheckBox)sender;
            //int value = dtCheckbox.IsChecked.Value ? 1 : -1;
            //string btnContent = ApproveSelectedTextBlock.Text;
            //int oldNumber = int.Parse(btnContent.Replace("อนุมัติ (", "").ToString().Replace(")", ""));
            //int newNumber = oldNumber + value;
            //ApproveSelectedTextBlock.Text = string.Format("อนุมัติ ({0})", newNumber);
        }

        private void ApproveButton_Click(object sender, RoutedEventArgs e)
        {
            List<OT> listModel = new List<OT>();
            var m = (OTViewmodel)OTDataGrid.SelectedItem;
            listModel.Add(new OT { Employee_ID = m.Employee_ID, OTDate = m.OTDate, OTType = m.OTType, RequestType = m.RequestType });
            Approve(listModel);
        }

        private void ApproveAllButton_Click(object sender, RoutedEventArgs e)
        {
            if (listFilter.Count <= 0) return;
            List<OT> listModel = new List<OT>();
            foreach (var m in listFilter)
            {
                listModel.Add(new OT { Employee_ID = m.Employee_ID, OTDate = m.OTDate, OTType = m.OTType, RequestType = m.RequestType });
            }
            Approve(listModel);
        }

        private void Approve(List<OT> listModel)
        {
            try
            {
                if (!(havID > 0))
                {
                    MessageBox.Show("ไม่สามารถบันทึก/แก้ไขข้อมูลได้ ผู้ใช้นี้ไม่ได้มีสิทธิ์เป็น HR Approver", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                if (listModel.Count <= 0) return;
                foreach (var i in listModel)
                {
                    if (!dt.UpdateOTDetail(i, "HR", "Approve"))
                    {
                        MessageBox.Show(string.Format("ผิดพลาด ไม่สามารถบันทึกข้อมูลได้ [{0}, {1}, {2}, {3}]", i.Employee_ID, i.OTDate, i.OTType, i.RequestType), "Error!");
                        return;
                    }
                }
                //Send Email
                //OTOnlineService.EmailSender().SendApproveEmail(listModel);
                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            ReloadDataGrid();
        }

        private void DeptCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (DeptCombobox.Text != "") ReloadDataGrid();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            NameTextBox.Text = string.Empty;
            TotalDocumentTextBox.Text = string.Empty;
            _searchedEmployee = null;
            DeptCombobox.Text = string.Empty;
            OTDataGrid.ItemsSource = null;
        }
    }
}
