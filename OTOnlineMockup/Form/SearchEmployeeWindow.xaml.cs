﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OTOnlineMockup.BLL;
using OTOnlineMockup.DAL;
using OTOnlineMockup.Model;
using OTOnlineMockup.ViewModel;

namespace OTOnlineMockup.Form
{
    /// <summary>
    /// Interaction logic for SearchEmployeeWindow.xaml
    /// </summary>
    public partial class SearchEmployeeWindow : Window
    {
        DataHelper dt = new DataHelper();
        string _requestFrom;
        public SearchEmployeeWindow(string requestFrom)
        {
            InitializeComponent();
            _requestFrom = requestFrom;
            SearchTypeCombobox.ItemsSource = dt.getList_SearchEmployeeType();
            StatusCombobox.ItemsSource = dt.getList_EmployeeStatus();
            StaffTypeCombobox.ItemsSource = dt.getList_StaffType();
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) GetData();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            //EmployeeInformationDataGrid.ItemsSource = null;
            //if (string.IsNullOrEmpty(SearchTextBox.Text)) return;

            //string searchString = SearchTextBox.Text.ToLower();
            //var listRawData = dt.getList_EmployeeInfomation(_requestFrom);
            //var listStatusFiltered = listRawData.Where(w => w.STATUS2 == StatusCombobox.Text).ToList();
            //var listStaffFiltered = listStatusFiltered.Where(w => w.Staff_type == StaffTypeCombobox.Text).ToList();

            //List<sp_OT_GetEmployeeInformation_Result> listFilteredData = new List<sp_OT_GetEmployeeInformation_Result>();
            //if      (SearchTypeCombobox.Text == "ค้นหาจากชื่อ (TH)") listFilteredData = listStaffFiltered.Where(w => w.FirstNameTH.Contains(searchString)).ToList();
            //else if (SearchTypeCombobox.Text == "ค้นหาจากชื่อ (EN)") listFilteredData = listStaffFiltered.Where(w => w.FirstNameEN.ToLower().Contains(searchString)).ToList();
            //else if (SearchTypeCombobox.Text == "ค้นหาจากนามสกุล (TH)") listFilteredData = listStaffFiltered.Where(w => w.LastNameTH.Contains(searchString)).ToList();
            //else if (SearchTypeCombobox.Text == "ค้นหาจากนามสกุล (EN)") listFilteredData = listStaffFiltered.Where(w => w.LastNameEN.ToLower().Contains(searchString)).ToList();
            //else if (SearchTypeCombobox.Text == "ค้นหาจากรหัส HR") listFilteredData = listStaffFiltered.Where(w => w.Employee_ID == searchString).ToList();
            //else if (SearchTypeCombobox.Text == "ค้นหาจากรหัส Payroll") listFilteredData = listStaffFiltered.Where(w => w.ACC_code == searchString).ToList();
            //else if (SearchTypeCombobox.Text == "ค้นหาจากรหัสบัตรประชาชน") listFilteredData = listStaffFiltered.Where(w => w.Card_ID == searchString).ToList();

            //EmployeeInformationDataGrid.ItemsSource = dt.convertToSearchedEmployee(listFilteredData);
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            if (EmployeeInformationDataGrid.SelectedIndex <= -1) return;
            var selectedData = (EmployeeInformationViewModel)EmployeeInformationDataGrid.SelectedItem;
            GlobalVariable.searchedEmployee = selectedData;
            Close();
        }

        private void SearchTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SearchTextBox.Focus();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SearchTypeCombobox.SelectedIndex = 0;
            StatusCombobox.SelectedIndex = 0;
            StaffTypeCombobox.SelectedIndex = 0;
        }
    }
}
