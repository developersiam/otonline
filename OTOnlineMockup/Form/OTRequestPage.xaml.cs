﻿using System;
using System.Windows;
using System.Windows.Controls;
using OTOnlineMockup.BLL;
using OTOnlineMockup.Model;
using OTOnlineMockup.ViewModel;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Input;
using OTOnlineMockup.DAL;
using OTOnlineMockup.Service;

namespace OTOnlineMockup.Form
{
    /// <summary>
    /// Interaction logic for OTRequestPage.xaml
    /// </summary>
    public partial class OTRequestPage : Page
    {
        DataHelper dt;
        int admID = 0;
        EmployeeInformationViewModel _searchedEmployee;
        public OTRequestPage()
        {
            InitializeComponent();
            dt = new DataHelper();
            admID = GlobalVariable.curentUser.Admin_ID.GetValueOrDefault();
            if (admID > 0) BindPayrollLot();
            else DisableUnpermit();
        }

        private void DisableUnpermit()
        {
            NameTextBox.IsEnabled = false;
            SearchButton.IsEnabled = false;
            ClearButton.IsEnabled = false;
            RequestNewOTButton.IsEnabled = false;
            LotToDateDatePicker.IsEnabled = false;
            LotFromDateDatePicker.IsEnabled = false;
            RefreshButton.IsEnabled = false;
        }

        private void BindPayrollLot()
        {
            var payrollLot = dt.getList_PayrollLot();
            LotCombobox.ItemsSource = payrollLot;
            LotCombobox.SelectedValue = "06/2018";
            //LotCombobox.SelectedValue = DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year;
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                OTDataGrid.ItemsSource = null;
                var selectedLot = (PayrolLotViewModel)LotCombobox.SelectedItem;
                DateTime displayDateStart = selectedLot.startDate;
                DateTime displayDateEnd = selectedLot.finishDate;
                LotFromDateDatePicker.DisplayDateStart = displayDateStart;
                LotFromDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotFromDateDatePicker.SelectedDate = displayDateStart;
                LotToDateDatePicker.DisplayDateStart = displayDateStart;
                LotToDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotToDateDatePicker.SelectedDate = displayDateEnd;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void ReloadDataGrid()
        {
            if (admID > 0)
            {
                OTDataGrid.ItemsSource = null;
                var listOTData = dt.getList_OT(LotCombobox.Text, "Admin");
                var listNameFilter = _searchedEmployee == null ? listOTData :
                                                                 listOTData.Where(w => w.Employee_ID == _searchedEmployee.EmployeeID).ToList();
                var listFilter = listNameFilter.Where(w => w.AdminApproveStatus != "A" &&
                                                           w.OTDate >= LotFromDateDatePicker.SelectedDate &&
                                                           w.OTDate <= LotToDateDatePicker.SelectedDate).ToList();
                OTDataGrid.ItemsSource = listFilter;
                TotalDocumentTextBox.Text = listFilter.Count().ToString("N0");
            }
            else MessageBox.Show("ชื่อผู้ใช้นี้ไม่ได้รับสิทธิให้ใช้งานสำหรับส่วนนี้", "คำเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void RequestNewOTButton_Click(object sender, RoutedEventArgs e)
        {
            if (admID > 0)
            {
                OTRequestDetailWindow window = new OTRequestDetailWindow(null, "Admin", "Insert");
                window.ShowDialog();
                ReloadDataGrid();
            }
            else MessageBox.Show("ชื่อผู้ใช้นี้ไม่ได้รับสิทธิให้ใช้งานสำหรับส่วนนี้", "คำเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            OTRequestDetailWindow window = new OTRequestDetailWindow((OTViewmodel)OTDataGrid.SelectedItem, "Admin", "Update");
            window.ShowDialog();
            ReloadDataGrid();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void NameTextBox_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Search();
        }

        private void Search()
        {

            if (admID > 0)
            {
                SearchEmployee_Window window = new SearchEmployee_Window("Admin");
                window.ShowDialog();
                if (window.selectedEmployee != null) NameTextBox.Text = window.selectedEmployee.Person.FirstNameEN + " " + window.selectedEmployee.Person.LastNameTH;
                //UpdateButton.IsEnabled = !string.IsNullOrEmpty(EmployeeIDTextBox.Text) || window.selectedEmployee != null;
                //SearchPositionButton.IsEnabled = !string.IsNullOrEmpty(EmployeeIDTextBox.Text) || window.selectedEmployee != null;

                //SearchEmployeeWindow window = new SearchEmployeeWindow("Admin");
                //window.ShowDialog();
                //if (GlobalVariable.searchedEmployee == null) return;
                //_searchedEmployee = GlobalVariable.searchedEmployee;
                //NameTextBox.Text = _searchedEmployee.NameTH;
                //GlobalVariable.searchedEmployee = null;
                ReloadDataGrid();
            }
            else MessageBox.Show("ชื่อผู้ใช้นี้ไม่ได้รับสิทธิให้ใช้งานสำหรับส่วนนี้", "คำเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        private void BindSelectedEmployee(string employee_ID)
        {
            throw new NotImplementedException();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            NameTextBox.Text = string.Empty;
            TotalDocumentTextBox.Text = string.Empty;
            _searchedEmployee = null;
            OTDataGrid.ItemsSource = null;
        }

        private void ApproveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!(admID > 0))
                {
                    MessageBox.Show("ไม่สามารถบันทึก/แก้ไขข้อมูลได้ ผู้ใช้นี้ไม่ได้มีสิทธิ์เป็น Admin", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (MessageBox.Show("ต้องการยืนยันการส่งคำร้องขอโอทีหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                var selected = (OTViewmodel)OTDataGrid.SelectedItem;
                OT model = new OT();
                model.Employee_ID = selected.Employee_ID;
                model.OTDate = selected.OTDate;
                model.OTType = selected.OTType;
                model.RequestType = selected.RequestType;
                if (dt.UpdateOTDetail(model, "Admin", "Approve"))
                {
                    //Send Email
                    List<OT> listOT = new List<OT>();
                    listOT.Add(model);
                    OTOnlineService.EmailSender().SendApproveEmail(listOT, "Admin");
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Information",MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            ReloadDataGrid();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!(admID > 0))
                {
                    MessageBox.Show("ไม่สามารถบันทึก/แก้ไขข้อมูลได้ ผู้ใช้นี้ไม่ได้มีสิทธิ์เป็น Admin", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
                var selected = (OTViewmodel)OTDataGrid.SelectedItem;
                OT model = new OT();
                model.Employee_ID = selected.Employee_ID;
                model.OTDate = selected.OTDate;
                model.OTType = selected.OTType;
                model.RequestType = selected.RequestType;
                if (!dt.DeleteOTDetail(model))
                {
                    MessageBox.Show("ผิดพลาด ไม่สามารถลบข้อมูลได้", "Error!");
                    return;
                }
                else
                {
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Information");
                }
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
