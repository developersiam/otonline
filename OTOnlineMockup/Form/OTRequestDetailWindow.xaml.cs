﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OTOnlineMockup.DAL;
using OTOnlineMockup.BLL;
using OTOnlineMockup.ViewModel;
using OTOnlineMockup.Model;
using System.Text.RegularExpressions;
using OTOnlineMockup.Service;

namespace OTOnlineMockup.Form
{
    /// <summary>
    /// Interaction logic for OTRequestDetailWindow.xaml
    /// </summary>
    public partial class OTRequestDetailWindow : Window
    {
        DataHelper dt = new DataHelper();
        Employee _searchedEmployee;
        string _formState = "";
        string _actionFrom = "";
        bool _isHoliday = true;

        public OTRequestDetailWindow()
        {
        }

        public OTRequestDetailWindow(OTViewmodel model, string actionfrom, string formState)
        {
            InitializeComponent();
            BindCombobox();
            _formState = formState;
            _actionFrom = actionfrom;

            HeaderTextblock.Text = string.Format("OT Request Detail ({0})", _formState);
            EmployeeIDTextBox.Text = _formState != "Insert" ? model.Employee_ID : "";
            OTTypeCombobox.IsEnabled = _formState != "Insert" ? false : true; // Disable if not Insert
            OTRequestCombobox.IsEnabled = _formState != "Insert" ? false : true; 
            //OTTextBox.IsEnabled = (_formState == "Insert" || _formState == "Update") ? true : false;
            OTHourCombobox.IsEnabled = (_formState == "Insert" || _formState == "Update") ? true : false;
            OTMinuteCombobox.IsEnabled = (_formState == "Insert" || _formState == "Update") ? true : false;
            RemarkAdminTextBox.IsEnabled = _actionFrom != "Admin" ? false : true; // Disable is not Admin
            RemarkManagerTextBox.IsEnabled = (_actionFrom == "Approver" && _formState == "Reject") ? true : false;
            RemarkFinalApvrTextBox.IsEnabled = (_actionFrom == "FinalApprover" && _formState == "Reject") ? true : false;
            RemarkHRTextBox.IsEnabled = (_actionFrom == "HR" && _formState == "Reject") ? true : false;

            SearchButton.Visibility = _formState == "Insert" ? Visibility.Visible : Visibility.Collapsed;
            //ApproveDetailButton.Visibility = ((_formState == "Update" || _formState == "View") && _actionFrom != "Report") ? Visibility.Visible : Visibility.Collapsed;
            ApproveDetailButton.Visibility = ( _formState == "View" && _actionFrom != "Report") ? Visibility.Visible : Visibility.Collapsed;
            SaveButton.Visibility = (_formState == "Update" || _formState == "Insert") ? Visibility.Visible : Visibility.Collapsed;
            RejectButton.Visibility = (_formState == "Reject") ? Visibility.Visible : Visibility.Collapsed;

            if (_formState == "Insert") return;
            _searchedEmployee = new Employee
            {
                Employee_ID = model.Employee_ID,
                Noted = model.AccountID,
                FingerScanID = model.FingerscanID
            };
            getOTDetail(model);
        }

        private void BindCombobox()
        {
            var payrollLot = dt.getList_PayrollLot();
            LotCombobox.ItemsSource = payrollLot;
            //Set selected lot to current month.
            //LotCombobox.SelectedValue = DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year;
            LotCombobox.SelectedValue = "06/2018";

            OTTypeCombobox.ItemsSource = dt.getList_OTType();
            OTRequestCombobox.ItemsSource = dt.getList_RequestType();

            BindOTHour();
            BindOTMinute();
        }

        private void BindOTHour()
        {
            //23 Apr 2019 Multiplier by HR requirement
            List<decimal> otHourList = new List<decimal>();
            for (decimal i = 0; i < 13; i++)
            {
                otHourList.Add(i);
            }
            OTHourCombobox.ItemsSource = null;
            OTHourCombobox.ItemsSource = otHourList;
            OTHourCombobox.SelectedIndex = 0;
        }

        private void BindOTMinute()
        {
            //23 Apr 2019 Multiplier by HR requirement
            List<OTMinutes> otMinuteList = new List<OTMinutes>();
            otMinuteList.Add(new OTMinutes { Minutes = "0", Multiplier = 0m });
            otMinuteList.Add(new OTMinutes { Minutes = "10", Multiplier = 0.167m });
            otMinuteList.Add(new OTMinutes { Minutes = "20", Multiplier = 0.33m });
            otMinuteList.Add(new OTMinutes { Minutes = "30", Multiplier = 0.50m });
            otMinuteList.Add(new OTMinutes { Minutes = "40", Multiplier = 0.67m });
            otMinuteList.Add(new OTMinutes { Minutes = "50", Multiplier = 0.83m });
            OTMinuteCombobox.ItemsSource = null;
            OTMinuteCombobox.ItemsSource = otMinuteList;
            OTMinuteCombobox.SelectedIndex = 0;
        }

        private void getOTDetail(OTViewmodel model)
        {
            var detail = dt.get_OTDetail(model);
            if (detail == null)
            {
                MessageBox.Show("ไม่พบข้อมูล", "Information", MessageBoxButton.OK);
                return;
            }
            EmployeeNameTextBox.Text = string.Format("{0} {1} {2}", detail.TitleNameTH, detail.FirstNameTH, detail.LastNameTH);
            LotCombobox.Text = detail.PayrollLot;
            OTDateDatePicker.SelectedDate = detail.OTDate;
            OTTypeCombobox.SelectedValue = detail.OTType;
            OTRequestCombobox.SelectedValue = detail.RequestType;
            OTTextBox.Text = detail.NcountOT.GetValueOrDefault().ToString("N1");
            string otAmount = detail.NcountOT.GetValueOrDefault().ToString();
            decimal otHour = Convert.ToDecimal(otAmount.Substring(0, 1));
            decimal otMimute = Convert.ToDecimal(otAmount) - otHour;
            OTHourCombobox.SelectedValue = otHour;
            OTMinuteCombobox.SelectedValue = otMimute;
            RemarkAdminTextBox.Text = detail.RemarkFromAdmin;
            RemarkManagerTextBox.Text = detail.RemarkFromManager;
            RemarkFinalApvrTextBox.Text = detail.RemarkFromFinalApprover;
            RemarkHRTextBox.Text = detail.RemarkFromHR;
            ApproveDetailButton.IsEnabled = detail.AdminApproveStatus == "A" ? false : true;
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var selectedLot = (PayrolLotViewModel)LotCombobox.SelectedItem;
                DateTime displayDateStart = selectedLot.startDate;
                DateTime displayDateEnd = selectedLot.finishDate;
                LotFromDateDatePicker.SelectedDate = displayDateStart;
                LotToDateDatePicker.SelectedDate = displayDateEnd;
                //Set OT range date to selected lot
                OTDateDatePicker.DisplayDateStart = displayDateStart;
                OTDateDatePicker.DisplayDateEnd = displayDateEnd;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EmployeeIDTextBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Search();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            if (_formState != "Insert") return;
            SearchEmployee_Window window = new SearchEmployee_Window("Admin");
            window.ShowDialog();
            if (window.selectedEmployee != null)
            {
                _searchedEmployee = window.selectedEmployee;
                EmployeeIDTextBox.Text = _searchedEmployee.Employee_ID;
                EmployeeNameTextBox.Text = _searchedEmployee.Person.FirstNameTH + " " + _searchedEmployee.Person.LastNameTH;
                OTDateDatePicker.IsEnabled = true;
            }
        }

        private void OTDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(EmployeeIDTextBox.Text) || string.IsNullOrEmpty(OTDateDatePicker.Text)) return;
            SetTimeInOut();
            SetDateType();
        }

        private void SetTimeInOut()
        {
            var time = dt.get_WorkingTime(OTDateDatePicker.SelectedDate.GetValueOrDefault(), _searchedEmployee);
            if (time == null) MessageBox.Show("ไม่พบเวลาทำงาน", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            ScanInTextBox.Text = time != null ? time.checkInTime : "";
            ScanOutTextBox.Text = time != null ? time.checkOutTime : "";
            TotalWorkTextBox.Text = time != null ? time.workingTime : "";
        }

        private void SetDateType()
        {   //If Sunday
            if (OTDateDatePicker.SelectedDate.GetValueOrDefault().DayOfWeek == DayOfWeek.Sunday)
            {
                HolidayTextBox.Text = "วันอาทิตย์"; OTTypeCombobox.SelectedValue = "H";
            }
            else
            {   //If Holiday
                var holiday = dt.get_Holiday(OTDateDatePicker.SelectedDate.Value);
                if (holiday != null)
                {
                    HolidayTextBox.Text = string.Format("{0} {1}", holiday.HolidayNameTH, (holiday.HolidayFlag.Value ? "(วันหยุดตามประกาศบริษัท)" : "(วันหยุดพิเศษ)"));
                    OTTypeCombobox.SelectedValue = "H";
                }
                else
                {
                    HolidayTextBox.Text = "วันทำงานปรกติ"; OTTypeCombobox.SelectedValue = "N";
                }
            }
            // True if Type is not Normal day
            _isHoliday = OTTypeCombobox.SelectedValue.ToString() != "N";
            //Set default to after work OT
            if (_formState != "Insert") return;
            OTRequestCombobox.SelectedIndex = 1;
            OTTextBox.Text = "";
            OTTextBox.Background = Brushes.White;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {   // Request new OT
            if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) Save();
        }

        private void Save()
        {
            try
            {
                var updateSuccess = false;
                if (DataValidate())
                {
                    var otHour = Convert.ToDecimal(OTHourCombobox.Text.ToString());
                    var otMinute = Convert.ToDecimal(OTMinuteCombobox.SelectedValue.ToString()); // Multiplier value
                    decimal requestedTime = otHour + otMinute;

                    OT model = new OT
                    {
                        Employee_ID = EmployeeIDTextBox.Text,
                        OTDate = Convert.ToDateTime(OTDateDatePicker.Text),
                        OTType = (string)OTTypeCombobox.SelectedValue,
                        RequestType = (string)OTRequestCombobox.SelectedValue,
                        ModifiedDate = DateTime.Now
                    };

                    if (_formState == "Insert")
                    {   // Create document by Admin
                        model.NcountOT = requestedTime;
                        model.Status = false;
                        model.Remark = "Create from OT Online";
                        model.PayrollLot = LotCombobox.Text;
                        model.RemarkFromAdmin = RemarkAdminTextBox.Text;

                        updateSuccess = dt.AddOTDetail(model);
                    }
                    else
                    {   // Update - Approve - Reject
                        // Admin.
                        model.NcountOT = requestedTime;
                        model.RemarkFromAdmin = RemarkAdminTextBox.Text;
                        // Manager. 
                        model.RemarkFromManager = RemarkManagerTextBox.Text;
                        //
                        model.RemarkFromFinalApprover = RemarkFinalApvrTextBox.Text;
                        // HR.
                        model.RemarkFromHR = RemarkHRTextBox.Text;

                        updateSuccess = dt.UpdateOTDetail(model, _actionFrom, _formState);
                    }

                    if (updateSuccess)
                    {
                        //Send Email
                        List<OT> listOT = new List<OT>();
                        listOT.Add(model);
                        //EmailSender email = new EmailSender();
                        if (_formState == "Approve") OTOnlineService.EmailSender().SendApproveEmail(listOT, _actionFrom);
                        if (_formState == "Reject") OTOnlineService.EmailSender().SendRejectEmail(model, _actionFrom);
                        MessageBox.Show(_formState == "Insert" ? "บันทึกข้อมูลสำเร็จ" : "อัพเดทข้อมูลสำเร็จ", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                        Close();
                    } 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private bool DataValidate()
        {
            if (!CheckRole())
            {
                MessageBox.Show("ไม่สามารถบันทึก/แก้ไขข้อมูลได้ ผู้ใช้นี้ไม่ได้มีสิทธิ์เป็น " + _actionFrom, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if (string.IsNullOrEmpty(EmployeeIDTextBox.Text) || string.IsNullOrEmpty(EmployeeNameTextBox.Text))
            {
                MessageBox.Show("กรุณาตรวจสอบข้อมูล, กรุณาเลือกพนักงาน", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (LotCombobox.Text == "" || LotFromDateDatePicker.Text == "" || LotToDateDatePicker.Text == "")
            {
                MessageBox.Show("กรุณาตรวจสอบข้อมูล, กรุณาระบุ Lot", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            //var selectedLot = (PayrolLotViewModel)LotCombobox.SelectedItem;
            //if (selectedLot.accountLocked)
            //{
            //    MessageBox.Show("ฝ่ายบัญชีได้ล๊อกงวดนี้แล้วจึงไม่สามารถเลือกงวดนี้ได้อีก", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}

            //if (selectedLot.hrLocked)
            //{
            //    MessageBox.Show("ฝ่าย HRM ได้ล๊อกงวดนี้ หากต้องการแก้ไขข้อมูลกรุณาปลดล๊อก", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            //    return false;
            //}

            if (OTDateDatePicker.Text == "")
            {
                MessageBox.Show("กรุณาตรวจสอบข้อมูล, กรุณาเลือกวันที่ทำ OT ", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (ScanInTextBox.Text == "" || ScanOutTextBox.Text == "")
            {
                MessageBox.Show("กรุณาตรวจสอบข้อมูล, ไม่พบเวลาการทำงานของพนักงาน", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (!OTAmountValidate())
            {
                MessageBox.Show("กรุณาตรวจสอบข้อมูล, จำนวนชั่วโมง OT ไม่ถูกต้อง", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            // Full time with break
            TimeSpan span = (Convert.ToDateTime(ScanOutTextBox.Text) - Convert.ToDateTime(ScanInTextBox.Text));
            //Case Normal Day : total working hour must >= 8 hour
            if (_isHoliday == false)
            {
                if (span.Hours < 8)
                {
                    MessageBox.Show("กรุณาตรวจสอบข้อมูล, เวลาการทำงานรวมของพนักงานไม่ถึง 8 ชั่วโมง", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
                //ให้คำนวนชั่วโมงกับนาทีที่เกิน  8 ชั่วโมง โดยให้คิดเป็นหน่วยนาที เพื่อเช็คกับจำนวนโอที user คีย์เข้าในระบบ
                decimal scanedTime = (((span.Hours - 8) * 60) + (span.Minutes)) - 30; // Subtract 30m for Break
                decimal otHour = Convert.ToDecimal(OTHourCombobox.Text.ToString());
                decimal otMinute = Convert.ToDecimal(OTMinuteCombobox.Text.ToString()); // Minute unit
                decimal requestedTime = ((otHour * 60) + otMinute);
                //decimal requestedTime = (Convert.ToDecimal(OTTextBox.Text) * 60);
                if (requestedTime > scanedTime)
                {
                    MessageBox.Show("กรุณาตรวจสอบข้อมูล, จำนวนโอทีที่คีย์มากกว่าที่ระบบคำนวนได้", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            if (OTTypeCombobox.Text == "")
            {
                MessageBox.Show("กรุณาตรวจสอบข้อมูล, กรุณาเลือกประเภท OT", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            //else if ((OTTypeCombobox.SelectedValue.ToString() == "H" || _isHoliday) && Convert.ToDecimal(OTTextBox.Text) > 2)
            else if (OTTypeCombobox.SelectedValue.ToString() == "H" && (Convert.ToDecimal(OTHourCombobox.Text.ToString()) + Convert.ToDecimal(OTMinuteCombobox.Text.ToString())) > 2)
            {
                MessageBox.Show("กรุณาตรวจสอบข้อมูล, วันทำงาน OT Holiday ไม่สามารถขอได้เกิน 2 ชั่วโมง", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (OTRequestCombobox.Text == "")
            {
                MessageBox.Show("กรุณาตรวจสอบข้อมูล, กรุณาเลือกช่วงเวลาทำ OT", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private bool CheckRole()
        {
            if (_actionFrom == "Admin" && GlobalVariable.curentUser.Admin_ID.GetValueOrDefault() > 0) return true;
            if (_actionFrom == "Approver" && GlobalVariable.curentUser.Approver_ID.GetValueOrDefault() > 0) return true;
            if (_actionFrom == "FinalApprover" && GlobalVariable.curentUser.FinalApprover_ID.GetValueOrDefault() > 0) return true;
            if (_actionFrom == "HR" && GlobalVariable.curentUser.HRApprover_ID.GetValueOrDefault() > 0) return true;
            return false;
        }

        private void OTTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //OTTextBox.Background = OTAmountValidate() ? Brushes.White : Brushes.LightPink;
        }

        private bool OTAmountValidate()
        {
            //string _otAmount = OTTextBox.Text.Trim();
            //if (string.IsNullOrEmpty(_otAmount)) return false; //return if null
            //Match match = Regex.Match(_otAmount, @"^[0-9]\d{0,1}(\.\d{1,1})?%?$"); // Regax 
            ////true if only number 0-9, one digit, is decimal, on digit decimal
            //bool allowCase = match.Success; 
            //bool allowNumber = false;
            ////true if more than 0 or decimal .0 or .5
            //if (allowCase) allowNumber = decimal.Parse(_otAmount) > 0 && (decimal.Parse(_otAmount) * 10) % 5 == 0; 
            //bool result = allowCase && allowNumber;
            //return result;
            if (OTHourCombobox.SelectedIndex < 0 || OTMinuteCombobox.SelectedIndex < 0) return false;
            var otHour = Convert.ToDecimal(OTHourCombobox.SelectedValue.ToString());
            var otMinute = Convert.ToDecimal(OTMinuteCombobox.SelectedValue.ToString());
            return otHour + otMinute > 0 ? true : false;
        }

        private void ApproveDetailButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("ยืนยันการ Approve หรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                _formState = "Approve";
                Save();
            }
        }

        private void RejectButton_Click(object sender, RoutedEventArgs e)
        {
            if (_formState != "Reject") return;
            if (MessageBox.Show("ยืนยันการ Reject หรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (_actionFrom == "Approver" && !string.IsNullOrEmpty(RemarkManagerTextBox.Text)) Save();
                else if (_actionFrom == "FinalApprover" && !string.IsNullOrEmpty(RemarkFinalApvrTextBox.Text)) Save();
                else if (_actionFrom == "HR" && !string.IsNullOrEmpty(RemarkHRTextBox.Text)) Save();
                else MessageBox.Show("กรุณาใส่ Remark การปฏิเสธ", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
