﻿using OTOnlineMockup.BLL;
using OTOnlineMockup.DAL;
using OTOnlineMockup.ViewModel;
using OTOnlineMockup.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OTOnlineMockup.Form
{
    /// <summary>
    /// Interaction logic for SearchEmployee_Window.xaml
    /// </summary>
    public partial class SearchEmployee_Window : Window
    {
        public Employee selectedEmployee;
        string _requestFrom;
        public SearchEmployee_Window(string requestFrom)
        {
            InitializeComponent();
            _requestFrom = requestFrom;
            SearchTypeCombobox.ItemsSource = BindSearchEmployeeTypeList();
            SearchTypeCombobox.SelectedIndex = 1;
            StatusCombobox.ItemsSource = OTOnlineService.DataHelper().getList_EmployeeStatus();
            StatusCombobox.SelectedValue = "1";
            StaffTypeCombobox.ItemsSource = OTOnlineService.DataHelper().getList_StaffType();
        }

        public List<string> BindSearchEmployeeTypeList()
        {
            List<string> result = new List<string>();
            result.Add("ค้นหาจากชื่อ (TH)");
            result.Add("ค้นหาจากชื่อ (EN)");
            result.Add("ค้นหาจากนามสกุล (TH)");
            result.Add("ค้นหาจากนามสกุล (EN)");
            result.Add("ค้นหาจากรหัสพนักงาน");
            //result.Add("ค้นหาจากรหัส HR");
            //result.Add("ค้นหาจากรหัส Payroll");
            result.Add("ค้นหาจากรหัสบัตรประชาชน");
            return result;
        }

        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ReloadDatagrid();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var searchStatus = (StaffStatu)StatusCombobox.SelectedItem;
                var searchStaff = (StaffType)StaffTypeCombobox.SelectedItem;

                string searchText = SearchTextBox.Text.Trim();
                string searchType = SearchTypeCombobox.Text;
                var indexStatus = searchStatus == null ? (int?)null : searchStatus.StaffStatusID;
                var indexStaff = searchStaff == null ? (int?)null : searchStaff.StaffTypeID;

                EmployeeInformationDataGrid.ItemsSource = null;
                EmployeeInformationDataGrid.ItemsSource = OTOnlineService.DataHelper().GetSearchedEmployee(searchType, indexStaff, indexStatus, searchText, _requestFrom);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            DataSelected();
        }

        private void EmployeeInformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) DataSelected();
        }

        private void DataSelected()
        {
            try
            {
                var employee = (Employee)EmployeeInformationDataGrid.SelectedItem;
                if (employee != null) selectedEmployee = employee; //Define data to public model for Passing value to mainpage.
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SearchTextBox.Focus();
        }

        private void StatusCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(StatusCombobox.Text)) ReloadDatagrid();
        }

        private void StaffTypeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(StaffTypeCombobox.Text)) ReloadDatagrid();
        }
    }
}
