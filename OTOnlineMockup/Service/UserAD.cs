﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices;
using OTOnlineMockup.Model;

namespace OTOnlineMockup.Service
{
    public interface IUserAD
    {
        bool Authenticate(string userName, string passWord);
    }
    public class UserAD : IUserAD
    {
        public bool Authenticate(string userName, string passWord)
        {
            try
            {
                PrincipalContext domain = new PrincipalContext(ContextType.Domain, null, userName, passWord);
                UserPrincipal user = UserPrincipal.FindByIdentity(domain, userName);
                DirectoryEntry directoryEntry = (user.GetUnderlyingObject() as DirectoryEntry);
                //Get Email
                GlobalVariable.userEmail = user.EmailAddress;
                if (directoryEntry != null)
                {
                    string[] directoryEntryPath = directoryEntry.Path.Split(',');
                    foreach (var splitedPath in directoryEntryPath)
                    {
                        string[] elements = splitedPath.Split('=');
                        if (elements[0].Trim() == "OU")
                        {
                            //Get Department
                            GlobalVariable.userDepartment = elements[1].Trim();
                            break;
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
