﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using OTOnlineMockup.Model;
using OTOnlineMockup.BLL;
using OTOnlineMockup.DAL;

namespace OTOnlineMockup.Service
{
    public interface IEmailSender
    {
        void SendApproveEmail(List<OT> listOT, string requestFrom);
        void SendRejectEmail(OT model, string requestFrom);
    }
    public class EmailSender : IEmailSender
    {
        public void SendApproveEmail(List<OT> listOT, string requestFrom)
        {
            string subject = "The OT request for approve from OT Online system.";
            var listDestination = OTOnlineService.DataHelper().getList_ApproveEmailDestination(listOT, requestFrom);
            if (listDestination == null || !listDestination.Any()) return;
            string messages = "<table>" +
                                    "<tr><td>ข้อความจากระบบ OT Online </td></tr>" +
                                    "<tr> <td>*** อีเมลฉบับนี้ถูกส่งผ่านระบบอัตโนมัติ กรุณาอย่าตอบกลับ ***</td></tr>" +
                                    "<tr> <td>คุณได้รับคำร้องให้พิจารณาการอนุมัติการขอทำโอทีจาก '" + requestFrom + "' จำนวน " + listOT.Count.ToString() + " รายการ </td></tr>" +
                                    "<tr> <td>กรุณาตรวจสอบข้อมูลและอนุมัติการขอทำโอทีของพนักงานจากระบบ OT Online </td></tr>" +
                              "</table>" +
                              "<br />" +
                              "<table>" +
                                    "<tr><td>Message from OT Online System.</td></tr>" +
                                    "<tr><td>*** This is an automatically generated email, please do not reply ***</td></tr>" +
                                    "<tr> <td>You have received " + listOT.Count.ToString() + " new request(s) OT online from '" + requestFrom + "' </td></tr>" +
                                    "<tr> <td>, please consider to approve on the OT Online system. </td></tr>" +
                              "</table>" +
                              "<br />" +
                              "<table>" +
                                    "<tr><td>Kind Regards,</td></tr>" +
                                    "<tr> <td>OT Online System</td></tr>" +
                              "</table>" +
                              "<br />";
            SendEmail(subject, messages, listDestination);
        }

        public void SendRejectEmail(OT model, string requestFrom)
        {
            try
            {
                string subject = "The OT request was rejected from OT Online system.";
                //string _userLevel = GlobalVariable.curentUser.FirstOrDefault().userLevel;
                var ots = OTOnlineService.DataHelper().get_OTDetail(new ViewModel.OTViewmodel { Employee_ID = model.Employee_ID, OTDate = model.OTDate, OTType = model.OTType, RequestType = model.RequestType });
                var listDestination = OTOnlineService.DataHelper().getList_RejectEmailDestination(model, requestFrom);
                if (listDestination == null || !listDestination.Any()) return;
                string otType = OTOnlineService.DataHelper().getList_OTType().SingleOrDefault(s => s.OTTypeCode == ots.OTType).OTTypeName;
                string requestType = OTOnlineService.DataHelper().getList_RequestType().SingleOrDefault(s => s.RequestFlag == ots.RequestType).RequestFlagName;
                string messages = "<table>" +
                                        "<tr> <td>ข้อความจากระบบ OT Online </td> </tr>" +
                                        "<tr> </tr>" +
                                        "<tr> <td>*** อีเมลฉบับนี้ถูกส่งผ่านระบบอัตโนมัติ กรุณาอย่าตอบกลับ ***</td> </tr>" +
                                        "<tr> <td>คำร้องการพิจารณาการอนุมัติการขอทำโอทีถูกปฏิเสธจาก '" + requestFrom + "' </td> </tr>" +
                                        "<tr> <td>Name : '" + ots.TitleNameTH + " " + ots.FirstNameTH + " " + ots.LastNameTH + "' </td> </tr>" +
                                        "<tr> <td>OT Date : '" + ots.OTDate.ToString("dd/MM/yyyy") + "' </td> </tr>" +
                                        "<tr> <td>OT Type : '" + otType + "' </td> </tr>" +
                                        "<tr> <td>Request Type : '" + requestType + "' </td> </tr>" +
                                        "<tr> <td>OT Amount : '" + ots.NcountOT.Value.ToString() + "' </td> </tr>" +
                                        "<tr> <td>Remark : '" + ots.Remark + "' </td> </tr>" +
                                        "<tr> <td>Admin Remark : '" + ots.RemarkFromAdmin + "' </td> </tr>" +
                                        "<tr> <td>Approver Remark : '" + ots.RemarkFromManager + "' </td> </tr>" +
                                        "<tr> <td>Final Approver Remark : '" + ots.RemarkFromFinalApprover + "' </td> </tr>" +
                                        "<tr> <td>HR Remark : '" + ots.RemarkFromHR + "' </td> </tr>" +
                                  "</table>" +
                                  "<br />" +
                                  "<table>" +
                                        "<tr> <td>Message from OT Online System.</td> </tr>" +
                                        "<tr> </tr>" +
                                        "<tr> <td>*** This is an automatically generated email, please do not reply ***</td> </tr>" +
                                        "<tr> <td> OT request was rejected by '" + requestFrom + "'</td> </tr>" +
                                        "<tr> <td>Name : '" + ots.FirstNameEN + " " + ots.LastNameEN + "' </td> </tr>" +
                                        "<tr> <td>OT Date : '" + ots.OTDate.ToString("dd/MM/yyyy") + "' </td> </tr>" +
                                        "<tr> <td>OT Type : '" + otType + "' </td> </tr>" +
                                        "<tr> <td>Request Type : '" + requestType + "' </td> </tr>" +
                                        "<tr> <td>OT Amount : '" + ots.NcountOT.Value.ToString() + "' </td> </tr>" +
                                        "<tr> <td>Remark : '" + ots.Remark + "' </td> </tr>" +
                                        "<tr> <td>Admin Remark : '" + ots.RemarkFromAdmin + "' </td> </tr>" +
                                        "<tr> <td>Approver Remark : '" + ots.RemarkFromManager + "' </td> </tr>" +
                                        "<tr> <td>Final Approver Remark : '" + ots.RemarkFromFinalApprover + "' </td> </tr>" +
                                        "<tr> <td>HR Remark : '" + ots.RemarkFromHR + "' </td> </tr>" +
                                  "</table>" +
                                  "<br />" +
                                  "<table>" +
                                        "<tr> <td>Kind Regards,</td> </tr>" +
                                        "<tr> <td>OT Online System</td> </tr>" +
                                  "</table>";
                SendEmail(subject, messages, listDestination);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SendEmail(string subject, string messages, List<Employee> receiverList)
        {
            try
            {
                string fromEmail = "it-info@siamtobacco.com";
                string fromPassword = "FvUfUp2uywvmu$tecE#";

                MailMessage mailmessage = new MailMessage();
                SmtpClient client = new SmtpClient();
                client.Credentials = new System.Net.NetworkCredential(fromEmail, fromPassword);
                client.Port = 587;
                client.Host = "smtp.emailsrvr.com";
                mailmessage.From = new MailAddress("it-info@siamtobacco.com");
                foreach (var address in receiverList)
                {
                    if (!string.IsNullOrEmpty(address.Email)) mailmessage.To.Add(address.Email);
                }
                mailmessage.CC.Add(GlobalVariable.userEmail);
                mailmessage.Subject = subject;
                mailmessage.Body = messages;
                mailmessage.IsBodyHtml = true;
                client.Send(mailmessage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
