﻿using OTOnlineMockup.BLL;

namespace OTOnlineMockup.Service
{
    public static class OTOnlineService
    {
        public static IDataHelper DataHelper()
        {
            DataHelper obj = new DataHelper();
            return obj;
        }
        public static IEmailSender EmailSender()
        {
            EmailSender obj = new EmailSender();
            return obj;
        }
        public static IUserAD UserAD()
        {
            UserAD obj = new UserAD();
            return obj;
        }
    }
}
