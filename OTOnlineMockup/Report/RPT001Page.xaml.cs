﻿using OTOnlineMockup.DAL;
using OTOnlineMockup.Form;
using OTOnlineMockup.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OTOnlineMockup.BLL;
using OTOnlineMockup.Model;
using OTOnlineMockup.Service;

namespace OTOnlineMockup.Report
{
    /// <summary>
    /// Interaction logic for RPT001Page.xaml
    /// </summary>
    public partial class RPT001Page : Page
    {
        Employee _searchedEmployee;
        public RPT001Page()
        {
            InitializeComponent();
            BindPayrollLot();
            DeptCombobox.ItemsSource = OTOnlineService.DataHelper().getList_Department("Report").OrderBy(o => o.Dept_code1);
            //StatusCombobox.ItemsSource = dt.get_DocumentStatus();
        }

        private void BindPayrollLot()
        {
            var payrollLot = OTOnlineService.DataHelper().getList_PayrollLot().Take(24);
            LotCombobox.ItemsSource = payrollLot;
            LotCombobox.SelectedValue = "06/2018";
            //LotCombobox.SelectedValue = DateTime.Now.Month.ToString("D2") + "/" + DateTime.Now.Year;
        }

        private void LotCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                OTDataGrid.ItemsSource = null;
                var selectedLot = (PayrolLotViewModel)LotCombobox.SelectedItem;
                DateTime displayDateStart = selectedLot.startDate;
                DateTime displayDateEnd = selectedLot.finishDate;
                LotFromDateDatePicker.DisplayDateStart = displayDateStart;
                LotFromDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotFromDateDatePicker.SelectedDate = displayDateStart;
                LotToDateDatePicker.DisplayDateStart = displayDateStart;
                LotToDateDatePicker.DisplayDateEnd = displayDateEnd;
                LotToDateDatePicker.SelectedDate = displayDateEnd;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void ReloadDataGrid()
        {
            OTDataGrid.ItemsSource = null;
            var listOTData = OTOnlineService.DataHelper().getList_OT(LotCombobox.Text, "Report");
            listOTData = listOTData.Where(w => w.OTDate >= LotFromDateDatePicker.SelectedDate &&
                                                       w.OTDate <= LotToDateDatePicker.SelectedDate).ToList();
            listOTData = _searchedEmployee == null ? listOTData :
                                                     listOTData.Where(w => w.Employee_ID == _searchedEmployee.Employee_ID).ToList();
            listOTData = string.IsNullOrEmpty(DeptCombobox.Text) ? listOTData :
                                                                    listOTData.Where(w => w.DeptCode == DeptCombobox.SelectedValue.ToString()).ToList();
            OTDataGrid.ItemsSource = listOTData;
            TotalDocumentTextBox.Text = listOTData.Count().ToString("N0");
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void NameTextBox_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Search();
        }

        private void Search()
        {
            SearchEmployee_Window window = new SearchEmployee_Window("Report");
            window.ShowDialog();
            if (window.selectedEmployee != null)
            {
                _searchedEmployee = window.selectedEmployee;
                NameTextBox.Text = _searchedEmployee.Person.FirstNameTH + " " + _searchedEmployee.Person.LastNameTH;
            }
            ReloadDataGrid();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            NameTextBox.Text = string.Empty;
            TotalDocumentTextBox.Text = string.Empty;
            DeptCombobox.Text = string.Empty;
            _searchedEmployee = null;
            OTDataGrid.ItemsSource = null;
        }

        private void DeptCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (DeptCombobox.Text != "") ReloadDataGrid();
        }

        private void ViewButton_Click(object sender, RoutedEventArgs e)
        {
            OTRequestDetailWindow window = new OTRequestDetailWindow((OTViewmodel)OTDataGrid.SelectedItem, "Report", "View");
            window.ShowDialog();
            ReloadDataGrid();
        }

        //private void StatusCombobox_DropDownClosed(object sender, EventArgs e)
        //{
        //    //if (StatusCombobox.Text != "") ReloadDataGrid();
        //}
    }
}
