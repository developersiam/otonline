﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.Model
{
    public class RequestFlags
    {
        public string RequestFlag { get; set; }
        public string RequestFlagName { get; set; }
    }
}
