﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OTOnlineMockup.ViewModel;

namespace OTOnlineMockup.Model
{
    public static class GlobalVariable
    {
        static string _userDepartment;
        public static string userDepartment
        {
            get
            {
                return _userDepartment;
            }
            set
            {
                _userDepartment = value;
            }
        }
        static string _userEmail;
        public static string userEmail
        {
            get
            {
                return _userEmail;
            }
            set
            {
                _userEmail = value;
            }
        }

        static EmployeeInformationViewModel _searchedEmployee;
        public static EmployeeInformationViewModel searchedEmployee
        {
            get
            {
                return _searchedEmployee;
            }
            set
            {
                _searchedEmployee = value;
            }
        }

        static  LoginUserViewModel _loginUser;
        public static LoginUserViewModel curentUser
        {
            get
            {
                return _loginUser;
            }
            set
            {
                _loginUser = value;
            }
        }
    }
}
