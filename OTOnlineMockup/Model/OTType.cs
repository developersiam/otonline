﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTOnlineMockup.Model
{
    public class OTType
    {
        public string OTTypeCode { get; set; }
        public string OTTypeName { get; set; }
    }
}
