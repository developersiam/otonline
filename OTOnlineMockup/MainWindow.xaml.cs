﻿using OTOnlineMockup.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OTOnlineMockup
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //if (GlobalVariable.curentUser.Count <= 0) return;
            Title = string.Format("OT Online System - [{0}]", FirstCapital(GlobalVariable.curentUser.userName));
            int hrApproverID = GlobalVariable.curentUser.HRApprover_ID.GetValueOrDefault();
            OTRequestMenu.Visibility = (GlobalVariable.curentUser.Admin_ID != null) ? Visibility.Visible : Visibility.Collapsed;
            ApproverMenu.Visibility = (GlobalVariable.curentUser.Approver_ID != null) ? Visibility.Visible : Visibility.Collapsed;
            FinalApproverMenu.Visibility = (GlobalVariable.curentUser.FinalApprover_ID != null) ? Visibility.Visible : Visibility.Collapsed;
            HRApproveMenu.Visibility = (GlobalVariable.curentUser.HRApprover_ID != null) ? Visibility.Visible : Visibility.Collapsed;
            SetupMenu.Visibility = (hrApproverID > 0 || GlobalVariable.userDepartment == "IT") ? Visibility.Visible : Visibility.Collapsed;

            //if (_userLevel == "Approver") this.MainFrame.Navigate(new Form.ManagerApprovePage());
            //else if (_userLevel == "FinalApprover") this.MainFrame.Navigate(new Form.DirectorApprove());
            //else if (_userLevel == "HR Approver") this.MainFrame.Navigate(new Form.HRApprovePage());
            //else this.MainFrame.Navigate(new Form.OTRequestPage());

            MainFrame.Navigate(new Report.RPT001Page());
        }

        private string FirstCapital(string Username)
        {
            if (string.IsNullOrEmpty(Username)) return string.Empty;
            // convert to char array of the string
            char[] letters = Username.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            return new string(letters);
        }

        private void ApproverMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.ManagerApprovePage());
        }

        private void OTRequestMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.OTRequestPage()); 
        }

        private void HRApproveMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.HRApprovePage());
        }

        private void RPT001Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT001Page());
        }

        private void FinalApproverMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.DirectorApprove());
        }

        private void AdminSetup_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.SetupPage());
        }
    }
}
